struct SpotLight
{
	vec3 position;
	vec3 color;
	vec3 direction;
	float power;
	float attenuation_constant;
	float attenuation_linear;
	float attenuation_quadratic;
	float phi;
	float theta;
	float gamma;
};
