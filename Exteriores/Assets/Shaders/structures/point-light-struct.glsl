struct PointLight
{
	vec3 position;
	vec3 color;
	float power;
	float attenuation_constant;
	float attenuation_linear;
	float attenuation_quadratic;
};
