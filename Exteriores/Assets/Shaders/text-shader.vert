#version 330 core

layout (location = 0) in vec4 vertices;

out vec2 v_uvs;

uniform mat4 MVP;

void main()
{
	v_uvs = vec2(vertices.z, vertices.w);
	
	gl_Position = MVP * vec4(vertices.x, vertices.y, 0.0, 1.0);
}