#version 330 core

layout (location = 0) in vec2 vertices;
layout (location = 1) in vec2 uvs;

out vec2 v_uvs;

uniform mat4 MVP;

void main()
{
	v_uvs = uvs;
	gl_Position = MVP * vec4(vertices, 0.0, 1.0);
}