#version 330 core

layout (location = 0) in vec3 vertices;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec2 uvs;

out vec3 v_vertices;
out vec3 v_normals;
out vec2 v_uvs;

uniform mat4 MVP;
uniform mat4 M;

uniform int flipnormals;

void main()
{
	v_vertices = vec3(M * vec4(vertices, 1.0));
	if(flipnormals > 0)
	{
		v_normals = mat3(transpose(inverse(M))) * -normals;
	}
	else
	{
		v_normals = mat3(transpose(inverse(M))) * normals;
	}
	v_uvs = uvs;
	
	gl_Position = MVP * vec4(vertices, 1.0);
}