#version 330 core

layout (location = 0) in vec2 vertices;
layout (location = 1) in vec2 uvs;

uniform mat4 MVP;

void main()
{
	gl_Position = MVP * vec4(vertices, 0.0, 1.0);
}