#version 330 core

out vec4 fragcolor;

uniform vec4 u_color;

void main()
{
	fragcolor = u_color;
}