#version 330 core

layout (location = 0) out vec4 output;

in vec2 v_uvs;

uniform vec2 resolution;
uniform sampler2D image;

void main()
{
    float PI2 = 6.28318530718;
    
    float directions = 16.0; 	// Def: 16.0
    float quality = 3.0; 		// Def: 3.0
    float size = 8.0; 			// Def: 8.0
	
    vec2 radius = size / resolution;
    vec4 color = texture(image, v_uvs);
    
    for(float d = 0.0; d < PI2; d += PI2 / directions)
    {
		for(float i = 1.0 / quality; i <= 1.0; i += 1.0 / quality)
        {
			color += texture(image, v_uvs + vec2(cos(d), sin(d)) * radius * i);
        }
    }
	
    color /= quality * directions - 15.0;
	output = color;
}