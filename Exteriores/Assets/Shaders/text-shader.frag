#version 330 core

in vec2 v_uvs;

out vec4 fragcolor;

uniform sampler2D u_character;
uniform vec4 u_color;

void main()
{
	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(u_character, v_uvs).r) * u_color;
	if(sampled.a == 0.0)
	{
		discard;
	}
	fragcolor = sampled;
}