#version 330 core

in vec2 v_uvs;

out vec4 fragcolor;

uniform vec4 color;

uniform sampler2D image;
uniform int hasimage;

void main()
{
	if(hasimage > 0)
	{
		fragcolor = texture(image, v_uvs) * color;
	}
	else
	{
		fragcolor = color;
	}
}