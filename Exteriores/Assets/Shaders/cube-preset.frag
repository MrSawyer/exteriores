#version 330 core

#include "structures/3d-material-struct.glsl"
#include "structures/point-light-struct.glsl"
#include "structures/spot-light-struct.glsl"

in vec3 v_vertices;
in vec3 v_normals;
in vec2 v_uvs;

out vec4 fragcolor;

uniform vec3 u_cameraposition;
uniform Material u_material;

uniform PointLight u_pointlights[4];
uniform int u_pointlights_count;

uniform SpotLight u_spotlights[4];
uniform int u_spotlights_count;

void main()
{
	vec4 albedo = u_material.color;
	if(u_material.hasimage != 0)
	{
		vec4 image = texture(u_material.image, v_uvs);
		albedo *= vec4(pow(image.rgb, vec3(2.2)), image.a);
	}

	vec3 ambient = vec3(0.0);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	
	vec3 normals = normalize(v_normals);
	
	for(int i = 0; i < u_pointlights_count; ++i)
	{
		vec3 lightdirection = normalize(u_pointlights[i].position - v_vertices);
		vec3 viewdirection = normalize(u_cameraposition - v_vertices);
		vec3 halfwaydirection = normalize(lightdirection + viewdirection);
		
		float diffuseforce = u_pointlights[i].power * clamp(dot(normals, lightdirection), 0.0, 1.0);
		float specularforce = u_pointlights[i].power * 0.5 * pow(clamp(dot(normals, halfwaydirection), 0.0, 1.0), 16.0) * diffuseforce;
		
		float distance = length(u_pointlights[i].position - v_vertices);
		float attenuation = 1.0 / (u_pointlights[i].attenuation_constant + u_pointlights[i].attenuation_linear * distance + u_pointlights[i].attenuation_quadratic * (distance * distance));
		
		ambient += 0.1 * albedo.rgb * attenuation;
		diffuse += diffuseforce * albedo.rgb * attenuation;
		specular += specularforce * vec3(1.0) * attenuation;
	}
	
	for(int i = 0; i < u_spotlights_count; ++i)
	{
		vec3 lightdirection = normalize(u_spotlights[i].position - v_vertices);
		
		float phi = cos(radians(u_spotlights[i].phi));
		float theta = dot(lightdirection, normalize(-u_spotlights[i].direction));
		float gamma = cos(radians(u_spotlights[i].gamma));
		float epsilon = phi - gamma;
		float intensity = clamp((theta - gamma) / epsilon, 0.0, 1.0);
		
		vec3 viewdirection = normalize(u_cameraposition - v_vertices);
		vec3 reflecteddirection = reflect(-lightdirection, normals);
		
		float diffuseforce = u_spotlights[i].power * clamp(dot(normals, lightdirection), 0.0, 1.0);
		float specularforce = u_spotlights[i].power * 0.5 * pow(clamp(dot(viewdirection, reflecteddirection), 0.0, 1.0), 16.0);
		
		float distance = length(u_spotlights[i].position - v_vertices);
		float attenuation = 1.0 / (u_spotlights[i].attenuation_constant + u_spotlights[i].attenuation_linear * distance + u_spotlights[i].attenuation_quadratic * (distance * distance));
		
		ambient += 0.1 * albedo.rgb * attenuation;
		diffuse += diffuseforce * albedo.rgb * intensity * attenuation;
		specular += specularforce * vec3(1.0) * intensity * attenuation;
	}
	
	fragcolor = vec4(ambient + diffuse + specular, albedo.a);
	fragcolor.rgb = pow(fragcolor.rgb, vec3(1.0 / 2.2));
}