#version 330 core

layout (location = 0) in vec2 vertices;
layout (location = 1) in vec2 uvs;

out vec2 v_uvs;

void main()
{
	v_uvs = uvs;
	gl_Position = vec4(2.0 * vertices, 0.0, 1.0);
}