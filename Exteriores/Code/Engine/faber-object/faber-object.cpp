#include <Engine/faber-object/faber-object.h>

namespace Fabr
{
	unsigned int count = 0;

	FaberObject::FaberObject()
	{
		count++;
		std::cout << "FaberObject()  " << count << std::endl;
	}

	FaberObject::~FaberObject()
	{
		count--;
		std::cout << "~FaberObject()  " << count << std::endl;
	}
}