#pragma once

#include <iostream>

namespace Fabr {
	class FaberObject
	{
	public:
		FaberObject();
		FaberObject(const FaberObject&) = default;
		FaberObject(FaberObject&&) = default;
		FaberObject& operator=(const FaberObject&) = default;
		FaberObject& operator=(FaberObject&&) = default;
		virtual ~FaberObject();
	};
}