#include <Engine/system/opengl/opengl-surface.h>
#include <Engine/system/window/instance.h>

namespace Fabr::System
{
	PIXELFORMATDESCRIPTOR OpenGLSurface::generatePixelFormatDescriptor()
	{
		PIXELFORMATDESCRIPTOR pfd = { 0 };
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.cColorBits = 24;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 24;
		pfd.cStencilBits = 8;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.iLayerType = PFD_MAIN_PLANE;
		FINF("Pixel format descriptor generated.");
		return pfd;
	}

	std::string OpenGLSurface::registerTemporaryClass()
	{
		WNDCLASS wc = { 0 };
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = Instance::getHandle();
		wc.hCursor = LoadCursor(0, IDC_ARROW);
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);
		wc.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		wc.lpfnWndProc = DefWindowProc;
		wc.lpszClassName = "ExterioresTMP";
		wc.lpszMenuName = nullptr;
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClass(&wc))
		{
			FERR("Registering temporary window class failed.");
			return std::string("");
		}

		FINF("Temporary window class registered.");
		return std::string("ExterioresTMP");
	}

	HWND OpenGLSurface::createTemporaryWindow(std::string classname)
	{
		HWND handleTMP = CreateWindow(classname.c_str(), "", WS_POPUP, 0, 0, 300, 300, 0, 0, Instance::getHandle(), nullptr);
		
		if (handleTMP == 0)
		{
			FERR("Creating temporary window failed.");
			return 0;
		}

		FINF("Temporary window created.");
		return handleTMP;
	}

	void OpenGLSurface::deleteTemporaryObjects(std::string classname, HWND handleTMP, HDC devTMP, HGLRC ctxTMP)
	{
		if (ctxTMP != 0)
		{
			wglMakeCurrent(0, 0);
			wglDeleteContext(ctxTMP);
		}
		if (devTMP != 0)
		{
			ReleaseDC(handleTMP, devTMP);
		}
		if (handleTMP != 0)
		{
			DestroyWindow(handleTMP);
		}
		if (!classname.empty())
		{
			UnregisterClass(classname.c_str(), Instance::getHandle());
		}
		FINF("Temporary objects deleted.");
	}

	bool OpenGLSurface::getAccessToOpenGLExtensions()
	{
		std::string classname = registerTemporaryClass();
		if (classname.empty())
		{
			deleteTemporaryObjects(classname);
			return false;
		}

		HWND handleTMP = createTemporaryWindow(classname);
		if (handleTMP == 0)
		{
			deleteTemporaryObjects(classname, handleTMP);
			return false;
		}

		PIXELFORMATDESCRIPTOR pfd = generatePixelFormatDescriptor();
		HDC devTMP = GetDC(handleTMP);

		int pf = ChoosePixelFormat(devTMP, &pfd);
		if (!SetPixelFormat(devTMP, pf, &pfd))
		{
			FERR("Setting temporary pixel format failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP);
			return false;
		}
		FINF("Temporary pixel format set.");

		HGLRC ctxTMP = wglCreateContext(devTMP);
		if (ctxTMP == 0)
		{
			FERR("Creating temporary OpenGL context failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}
		FINF("Temporary OpenGL context created.");

		if (!wglMakeCurrent(devTMP, ctxTMP))
		{
			FERR("Making temporary OpenGL context current failed.");
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}
		FINF("Temporary OpenGL context current.");

		if (!loadOpenGLExtensions())
		{
			deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);
			return false;
		}

		deleteTemporaryObjects(classname, handleTMP, devTMP, ctxTMP);

		return true;
	}

	bool OpenGLSurface::loadOpenGLExtensions()
	{
		if (glewInit() != GLEW_OK)
		{
			FERR("Loading OpenGL extensions failed.");
			return false;
		}
		FINF("OpenGL extensions loaded.");
		return true;
	}

	bool OpenGLSurface::createOpenGLContext()
	{
		if (!getAccessToOpenGLExtensions())
		{
			PostQuitMessage(2);
			return false;
		}

		PIXELFORMATDESCRIPTOR	pfd = generatePixelFormatDescriptor();
		int						pf = 0;
		unsigned int			pfc = 0;

		const int dev_attributes[] =
		{
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
			WGL_COLOR_BITS_ARB, 24,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 24,
			WGL_STENCIL_BITS_ARB, 8,
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
			WGL_SAMPLES_ARB, 8,
			0
		};

		const int ctx_attributes[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 3,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0
		};

		if (!wglChoosePixelFormatARB(dev, dev_attributes, nullptr, 1, &pf, &pfc))
		{
			FERR("Choosing pixel format failed.");
			PostQuitMessage(3);
			return false;
		}

		if (!SetPixelFormat(dev, pf, &pfd))
		{
			FERR("Setting pixel format failed.");
			PostQuitMessage(3);
			return false;
		}
		FINF("Pixel format set.");

		ctx = wglCreateContextAttribsARB(dev, 0, ctx_attributes);
		if (ctx == 0)
		{
			FERR("OpenGL 3.3 not supported.");
			MessageBox(handle, "OpenGL 3.3 is not supported by your graphics card.", "Critical Error", MB_ICONERROR | MB_OK);
			PostQuitMessage(3);
			return false;
		}
		FINF("OpenGL 3.3 context created.");

		if (!wglMakeCurrent(dev, ctx))
		{
			FERR("Making OpenGL context current failed.");
			PostQuitMessage(3);
			return false;
		}
		FINF("OpenGL context current.");

		return true;
	}

	void OpenGLSurface::deleteOpenGLContext()
	{
		if (ctx != 0)
		{
			wglMakeCurrent(0, 0);
			wglDeleteContext(ctx);
			ctx = 0;
		}
		if (dev != 0)
		{
			ReleaseDC(handle, dev);
			dev = 0;
		}
		FINF("OpenGL 3.3 context deleted.");
	}

	void OpenGLSurface::setOpenGLSettings()
	{
		//wglSwapIntervalEXT(0);

		setBackgroundColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
		setDepthClearValue(1.0);
		setStencilClearValue(0);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_MULTISAMPLE);
	}

	OpenGLSurface::OpenGLSurface(HWND owner)
	{
		FASSERT_MSG(owner != 0, "OpenGLSurface owner cannot be null.");

		handle = owner;
		dev = GetDC(handle);
		ctx = 0;

		if (!createOpenGLContext()) deleteOpenGLContext();
		else setOpenGLSettings();
	}

	OpenGLSurface::~OpenGLSurface()
	{
		deleteOpenGLContext();
	}

	bool OpenGLSurface::isGood() const
	{
		return (handle != 0) && (dev != 0) && (ctx != 0);
	}

	bool OpenGLSurface::makeCurrent() const
	{
		return wglMakeCurrent(dev, ctx);
	}

	void OpenGLSurface::setAsRenderTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void OpenGLSurface::clearBuffers(SurfaceMask masks)
	{
		glClear(static_cast<GLbitfield>(masks));
	}

	void OpenGLSurface::swapBuffers()
	{
		SwapBuffers(dev);
	}

	void OpenGLSurface::setBackgroundColor(glm::vec4 color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGLSurface::setDepthClearValue(double depth)
	{
		glClearDepth(depth);
	}

	void OpenGLSurface::setStencilClearValue(int stencil)
	{
		glClearStencil(stencil);
	}

	void OpenGLSurface::enableDepthTest(bool enable)
	{
		if (enable) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
	}

	void OpenGLSurface::enableStencilTest(bool enable)
	{
		if (enable) glEnable(GL_STENCIL_TEST);
		else glDisable(GL_STENCIL_TEST);
	}
}