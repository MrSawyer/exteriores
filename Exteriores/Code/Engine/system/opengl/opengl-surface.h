#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <Engine/system/opengl/surface-bitfields.h>

#include <string>
#include <Windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLM/glm.hpp>

namespace Fabr::System
{
	class OpenGLSurface : public virtual FaberObject
	{
	private:
		HWND	handle;
		HDC		dev;
		HGLRC	ctx;

	protected:
		PIXELFORMATDESCRIPTOR generatePixelFormatDescriptor();

	protected:
		std::string	registerTemporaryClass();
		HWND		createTemporaryWindow(std::string classname);
		void		deleteTemporaryObjects(std::string classname, HWND handleTMP = 0, HDC devTMP = 0, HGLRC ctxTMP = 0);

	protected:
		bool getAccessToOpenGLExtensions();
		bool loadOpenGLExtensions();
		bool createOpenGLContext();
		void deleteOpenGLContext();

	protected:
		void setOpenGLSettings();

	public:
		OpenGLSurface(HWND owner);
		OpenGLSurface(const OpenGLSurface& other) = delete;
		~OpenGLSurface();

	public:
		bool isGood() const;
		bool makeCurrent() const;

	public:
		void setAsRenderTarget();
		void clearBuffers(SurfaceMask masks);
		void swapBuffers();

	public:
		void setBackgroundColor(glm::vec4 color);
		void setDepthClearValue(double depth);
		void setStencilClearValue(int stencil);

	public:
		void enableDepthTest(bool enable);
		void enableStencilTest(bool enable);
	};
}