#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <winnt.h>

namespace Fabr::System
{
	enum class SurfaceMask : unsigned int
	{
		COLOR_BUFFER = GL_COLOR_BUFFER_BIT,
		DEPTH_BUFFER = GL_DEPTH_BUFFER_BIT,
		STENCIL_BUFFER = GL_STENCIL_BUFFER_BIT
	};
}

DEFINE_ENUM_FLAG_OPERATORS(Fabr::System::SurfaceMask)