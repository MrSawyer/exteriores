#pragma once

#include <Windows.h>

namespace Fabr::System
{
	class Instance
	{
	private:
		static HINSTANCE g_handle;

	public:
		static void setHandle(HINSTANCE instance);
		static const HINSTANCE& getHandle();

	protected:
		Instance() = delete;
		Instance(const Instance&) = delete;
		~Instance() = delete;
	};
}