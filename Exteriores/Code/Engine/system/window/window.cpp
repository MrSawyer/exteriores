#include <Engine/system/window/window.h>
#include <Engine/system/window/instance.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Engine/flog/flog.h>
#include <windowsx.h>

#include <Resources/resource.h>

namespace Fabr::System
{
	void Window::onResize(int width, int height)
	{
		clientwidth = static_cast<float>(width);
		clientheight = static_cast<float>(height);

		glViewport(0, 0, width, height);
	}

	LRESULT __stdcall Window::g_windowCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		Window* pointer = nullptr;
		if (message == WM_NCCREATE)
		{
			CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>(lparam);
			pointer = reinterpret_cast<Window*>(cs->lpCreateParams);

			SetLastError(0);
			bool success = SetWindowLongPtr(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pointer)) != 0;
			if (!success && GetLastError() != 0)
			{
				FERR("Initialization of window class callback failed.");
				PostQuitMessage(1);
				return 0;
			}
			FINF("Window class callback initialized.");
		}
		else
		{
			pointer = reinterpret_cast<Window*>(GetWindowLongPtr(window, GWLP_USERDATA));
		}

		return pointer != nullptr ? pointer->windowCallback(window, message, wparam, lparam) : DefWindowProc(window, message, wparam, lparam);
	}

	LRESULT __stdcall Window::windowCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		switch (message)
		{
		case WM_MOUSEMOVE:
		{
			cursorpositionX = static_cast<float>(GET_X_LPARAM(lparam));
			cursorpositionY = static_cast<float>(GET_Y_LPARAM(lparam));
			break;
		}

		case WM_KEYDOWN:
		{
			input->setKeyboardButtonState((int)wparam, true);
			input->setKeyCode((int)wparam);
			break;
		}

		case WM_KEYUP:
		{
			input->setKeyboardButtonState((int)wparam, false);
			break;
		}

		case WM_LBUTTONDOWN:
		{
			input->setMouseButtonState(0, true);
			break;
		}

		case WM_LBUTTONUP:
		{
			input->setMouseButtonState(0, false);
			break;
		}

		case WM_RBUTTONDOWN:
		{
			input->setMouseButtonState(1, true);
			break;
		}

		case WM_RBUTTONUP:
		{
			input->setMouseButtonState(1, false);
			break;
		}

		case WM_SIZE:
		{
			int width = static_cast<int>(LOWORD(lparam));
			int height = static_cast<int>(HIWORD(lparam));

			if (!fullscreen)
			{
				if (width != 0 && height != 0)
				{
					onResize(width, height);
					if (pipeline != nullptr)
					{
						glm::mat4 orthographic_projection = glm::ortho(-clientwidth / 2.0f, clientwidth / 2.0f, -clientheight / 2.0f, clientheight / 2.0f);
						glm::mat4 perspective_projection = glm::infinitePerspective(glm::radians(60.0f), clientwidth / clientheight, 0.1f);

						PipelineTickVariables ptv;
						ptv.width = &clientwidth;
						ptv.height = &clientheight;
						ptv.orthographic = &orthographic_projection;
						ptv.perspective = &perspective_projection;

						PTVGlobalContainer::setPipelineResizeVariables(ptv);
						pipeline->onResize(clientwidth, clientheight);
					}
				}
			}

			return 0;
		}

		case WM_ERASEBKGND:
		{
			return TRUE;
		}

		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

		case WM_SYSCOMMAND:
		{
			switch (wparam)
			{
			case SC_SCREENSAVE:
				return 0;

			case SC_MONITORPOWER:
				return 0;
			}
			break;
		}
		}

		return DefWindowProc(window, message, wparam, lparam);
	}

	bool Window::registerClass()
	{
		WNDCLASS wc = { 0 };
		bool registered = GetClassInfo(Instance::getHandle(), "Exteriores", &wc);
		if (registered)
		{
			return true;
		}

		wc = { 0 };
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = Instance::getHandle();
		wc.hCursor = LoadCursor(Instance::getHandle(), MAKEINTRESOURCE(IDC_CURSOR1));
		wc.hIcon = LoadIcon(Instance::getHandle(), MAKEINTRESOURCE(IDI_ICON1));
		wc.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		wc.lpfnWndProc = g_windowCallback;
		wc.lpszClassName = "Exteriores";
		wc.lpszMenuName = nullptr;
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		return static_cast<bool>(RegisterClass(&wc));
	}

	bool Window::createWindow()
	{
		if (fullscreen)
		{
			int width = GetSystemMetrics(SM_CXSCREEN);
			int height = GetSystemMetrics(SM_CYSCREEN);

			handle = CreateWindowEx(WS_EX_APPWINDOW, "Exteriores", "Exteriores", WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0, 0, width, height, 0, 0, Instance::getHandle(), reinterpret_cast<LPVOID>(this));
			
			onResize(width, height);
		}
		else
		{
			handle = CreateWindowEx(WS_EX_APPWINDOW | WS_EX_WINDOWEDGE, "Exteriores", "Exteriores", WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_MAXIMIZE, CW_USEDEFAULT, CW_USEDEFAULT, 1200, 700, 0, 0, Instance::getHandle(), reinterpret_cast<LPVOID>(this));
		}
		return isGood();
	}

	Window::Window()
	{
		handle = 0;
		if (MessageBox(0, "Would you like to launch Exteriores on fullscreen mode?", "Exteriores", MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
			fullscreen = true;
		}
		else
		{
			fullscreen = false;
		}

		if (!registerClass())
		{
			FERR("Registering window class failed.");
			PostQuitMessage(1);
			return;
		}
		else if (!createWindow())
		{
			FINF("Window class registered.");
			FERR("Creating window failed.");
			PostQuitMessage(1);
			return;
		}

		FINF("Window class registered.");
		FINF("Window created.");

		input = std::make_unique<Input>();
		surface = std::make_unique<OpenGLSurface>(handle);
	}

	Window::~Window()
	{
		if (!pipeline->onTerminate(0))
		{
			FWAR("Termination event returned false.");
		}
		else
		{
			FINF("Termination event succeed.");
		}

		surface.reset();
		input.reset();

		if(isGood())
		{
			DestroyWindow(handle);
			handle = 0;
			FINF("Window deleted.");
		}
	}

	bool Window::isGood() const
	{
		return handle != 0;
	}

	void Window::show()
	{
		ShowWindow(handle, SW_SHOW);
		UpdateWindow(handle);
		FINF("Show window command.");
	}

	void Window::hide()
	{
		ShowWindow(handle, SW_HIDE);
		FWAR("Hide window command.");
	}

	void Window::close()
	{
		PostMessage(handle, WM_CLOSE, 0, 0);
		FWAR("PostMessage WM_CLOSE sent.");
	}

	int Window::launchLoop()
	{
		if (pipeline != nullptr)
		{
			if (!pipeline->onInitialize(0))
			{
				FWAR("Initialization event returned false.");
			}
			else
			{
				FINF("Initialization event succeed.");
			}
			pipeline->setInitializationEvent(0);
			if (fullscreen)
			{
				glm::mat4 orthographic_projection = glm::ortho(-clientwidth / 2.0f, clientwidth / 2.0f, -clientheight / 2.0f, clientheight / 2.0f);
				glm::mat4 perspective_projection = glm::infinitePerspective(glm::radians(60.0f), clientwidth / clientheight, 0.1f);

				PipelineTickVariables ptv;
				ptv.width = &clientwidth;
				ptv.height = &clientheight;
				ptv.orthographic = &orthographic_projection;
				ptv.perspective = &perspective_projection;

				PTVGlobalContainer::setPipelineResizeVariables(ptv);
				pipeline->onResize(clientwidth, clientheight);
			}
			show();
		}

		__int64 CPS = 0;
		__int64 pT = 0;
		__int64 cT = 0;
		double	dT = 0.0;

		QueryPerformanceFrequency((LARGE_INTEGER*)&CPS);
		QueryPerformanceCounter((LARGE_INTEGER*)&pT);

		double SPC = 1.0 / CPS;

		float maxFPS = 144.0f;
		double minFrameT = 1.0 / (double)maxFPS;

		bool checkevents = true;

		MSG msgcontainer = { 0 };
		while (msgcontainer.message != WM_QUIT)
		{
			if (PeekMessage(&msgcontainer, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msgcontainer);
				DispatchMessage(&msgcontainer);
			}
			else
			{
				if (pipeline != nullptr)
				{
					QueryPerformanceCounter((LARGE_INTEGER*)&cT);
					dT += (cT - pT) * SPC;
					pT = cT;

					PipelineTickVariables ptv;
					ptv.window = this;
					ptv.input = input.get();
					ptv.surface = surface.get();
					ptv.maxFPS = &maxFPS;

					PTVGlobalContainer::setPipelineTickVariables(ptv);

					while (dT > minFrameT)
					{
						CursorPositionCalculationVariables cpcv;
						cpcv.clientsize.x = clientwidth;
						cpcv.clientsize.y = clientheight;
						cpcv.cursorposition.x = cursorpositionX;
						cpcv.cursorposition.y = cursorpositionY;

						input->calculateCursorPositions(cpcv);

						int eventcode = pipeline->shouldInitialize();
						if (eventcode != 0)
						{
							if (!pipeline->onInitialize(eventcode))
							{
								FWAR("Initialization event returned false.");
							}
							else
							{
								FINF("Initialization event succeed.");
							}
							pipeline->setInitializationEvent(0);
						}

						eventcode = pipeline->shouldTerminate();
						if (eventcode != 0)
						{
							if (!pipeline->onTerminate(eventcode))
							{
								FWAR("Termination event returned false.");
							}
							else
							{
								FINF("Termination event succeed.");
							}
							pipeline->setTerminationEvent(0);
						}

						eventcode = pipeline->shouldChange();
						if (eventcode != 0)
						{
							if (!pipeline->onChange(eventcode))
							{
								FWAR("Change event return false.");
							}
							else
							{
								FINF("Change event succeed.");
							}
							pipeline->setChangeEvent(0);
						}

						pipeline->onLogicTick();
						input->zeroClickedOrReleasedKeys();

						dT -= minFrameT;
					}

					pipeline->onRendererTick();
				}
				else
				{
					QueryPerformanceCounter((LARGE_INTEGER*)&pT);
				}
			}
		}
		return static_cast<int>(msgcontainer.wParam);
	}

	void Window::setPipeline(Pipeline* pipeline)
	{
		this->pipeline = pipeline;
		FINF("Pipeline set.");
	}

	const HWND& Window::getHandle() const
	{
		return handle;
	}
}