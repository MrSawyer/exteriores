#include <Engine/system/window/instance.h>
#include <Engine/flog/flog.h>

namespace Fabr::System
{
	HINSTANCE Instance::g_handle = 0;

	void Instance::setHandle(HINSTANCE instance)
	{
		if (g_handle == 0)
		{
			g_handle = instance;
			FINF("Instance handle save.");
		}
	}

	const HINSTANCE& Instance::getHandle()
	{
		return g_handle;
	}
}