#pragma once

#include <Engine/faber-object/faber-object.h>
#include <Engine/system/input/input.h>
#include <Engine/system/opengl/opengl-surface.h>
#include <Engine/system/pipeline/pipeline.h>

#include <memory>
#include <Windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>

namespace Fabr::System
{
	class Window : public virtual FaberObject
	{
	private:
		HWND handle;
		bool fullscreen = false;

	private:
		float clientwidth, clientheight;
		float cursorpositionX, cursorpositionY;

	protected:
		void onResize(int width, int height);

	protected:
		static LRESULT __stdcall g_windowCallback(HWND, UINT, WPARAM, LPARAM);
		LRESULT __stdcall windowCallback(HWND, UINT, WPARAM, LPARAM);

	protected:
		bool registerClass();
		bool createWindow();

	private:
		std::unique_ptr<Input> input;
		std::unique_ptr<OpenGLSurface> surface;

	private:
		Pipeline*	pipeline;

	public:
		Window();
		Window(const Window& other) = delete;
		~Window();

	protected:
		void show();
		void hide();

	public:
		bool isGood() const;
		void close();

	public:
		int launchLoop();

	public:
		void setPipeline(Pipeline* pipeline);

	public:
		const HWND& getHandle() const;
	};
}