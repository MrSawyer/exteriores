#include <Engine/system/input/input.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

namespace Fabr::System
{
	CursorPositionCalculationVariables::CursorPositionCalculationVariables()
	{
		cursorposition = glm::vec2(0.0f, 0.0f);
		clientsize = glm::vec2(0.0f, 0.0f);
	}

	Input::Input()
	{
		cp2d = glm::vec2(0.0f, 0.0f);
		cp3d = glm::vec3(0.0f, 0.0f, 0.0f);
		kbchartyped = 0;
	}

	Input::~Input() {}

	glm::vec2 Input::getCursorPosition() const
	{
		return cp2d;
	}

	float Input::getCursorPositionX() const
	{
		return cp2d.x;
	}

	float Input::getCursorPositionY() const
	{
		return cp2d.y;
	}

	glm::vec3 Input::getCursorDirection() const
	{
		return cp3d;
	}

	float Input::getCursorDirectionX() const
	{
		return cp3d.x;
	}

	float Input::getCursorDirectionY() const
	{
		return cp3d.y;
	}

	float Input::getCursorDirectionZ() const
	{
		return cp3d.z;
	}

	bool Input::keyPressed(int key) const
	{
		return kbpressed[key];
	}

	bool Input::keyPressed(char key) const
	{
		return kbpressed[(int)key];
	}

	bool Input::keyPressed(MouseButtons key) const
	{
		return mbpressed[static_cast<int>(key)];
	}

	bool Input::keyClicked(int key) const
	{
		return kbclicked[key];
	}

	bool Input::keyClicked(char key) const
	{
		return kbclicked[(int)key];
	}

	bool Input::keyClicked(MouseButtons key) const
	{
		return mbclicked[static_cast<int>(key)];
	}

	bool Input::keyReleased(int key) const
	{
		return kbreleased[key];
	}

	bool Input::keyReleased(char key) const
	{
		return kbreleased[(int)key];
	}

	bool Input::keyReleased(MouseButtons key) const
	{
		return mbreleased[static_cast<int>(key)];
	}

	void Input::setKeyboardButtonState(int key, bool state)
	{
		if (kbpressed[key] == false && state == true)
		{
			kbclicked[key] = true;
		}
		else if (kbpressed[key] == true && state == false)
		{
			kbreleased[key] = true;
		}
		kbpressed[key] = state;
	}

	void Input::setMouseButtonState(int key, bool state)
	{
		if (mbpressed[key] == false && state == true)
		{
			mbclicked[key] = true;
		}
		else if (mbpressed[key] == true && state == false)
		{
			mbreleased[key] = true;
		}
		mbpressed[key] = state;
	}

	void Input::calculateCursorPositions(CursorPositionCalculationVariables& v)
	{
		cp2d.x = v.cursorposition.x - v.clientsize.x / 2.0f;
		cp2d.y = v.clientsize.y / 2.0f - v.cursorposition.y;

		cp3d.x = cp2d.x / v.clientsize.x * 2.0f;
		cp3d.y = cp2d.y / v.clientsize.y * 2.0f;

		Graphics::Camera3D* camera = PTVGlobalContainer::getActiveCamera3D();
		if (camera != nullptr)
		{
			glm::mat4 invVP = glm::inverse(PTVGlobalContainer::getPerspectiveProjection() * PTVGlobalContainer::getActiveCamera3D()->getMatrix());
			glm::vec4 S = glm::vec4(cp3d.x, cp3d.y, 1.0f, 1.0f);
			glm::vec4 W = invVP * S;
			cp3d = glm::normalize(W);
		}
		else
		{
			glm::mat4 invVP = glm::inverse(PTVGlobalContainer::getPerspectiveProjection());
			glm::vec4 S = glm::vec4(cp3d.x, cp3d.y, 1.0f, 1.0f);
			glm::vec4 W = invVP * S;
			cp3d = glm::normalize(W);
		}
	}

	void Input::zeroClickedOrReleasedKeys()
	{
		for (int i = 0; i < 256; ++i)
		{
			kbclicked[i] = false;
			kbreleased[i] = false;
		}

		for (int i = 0; i < 2; ++i)
		{
			mbclicked[i] = false;
			mbreleased[i] = false;
		}

		kbchartyped = 0;
	}

	void Input::setKeyCode(int vkcode)
	{
		if ((vkcode >= 'A' && vkcode <= 'Z') || (vkcode >= '0' && vkcode <= '9') || (vkcode == ' '))
		{
			kbchartyped = vkcode;
		}
	}

	char Input::getCharTyped() const
	{
		return (char)kbchartyped;
	}
}