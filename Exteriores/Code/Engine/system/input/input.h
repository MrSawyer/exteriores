#pragma once

#include <Engine/faber-object/faber-object.h>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

namespace Fabr::System
{
	struct CursorPositionCalculationVariables
	{
		glm::vec2 cursorposition;
		glm::vec2 clientsize;

		CursorPositionCalculationVariables();
	};

	enum class MouseButtons
	{
		LEFT_BUTTON = 0,
		RIGHT_BUTTON = 1
	};

	class Input : public virtual FaberObject
	{
	private:
		glm::vec2 cp2d;
		glm::vec3 cp3d;
		int kbchartyped;
		bool kbpressed[256] = { 0 }, kbclicked[256] = { 0 }, kbreleased[256] = { 0 };
		bool mbpressed[2] = { 0 }, mbclicked[2] = { 0 }, mbreleased[2] = { 0 };

	public:
		Input();
		Input(const Input& other) = delete;
		~Input();

	public:
		glm::vec2	getCursorPosition() const;
		float		getCursorPositionX() const;
		float		getCursorPositionY() const;

	public:
		glm::vec3	getCursorDirection() const;
		float		getCursorDirectionX() const;
		float		getCursorDirectionY() const;
		float		getCursorDirectionZ() const;

	public:
		bool keyPressed(int key) const;
		bool keyPressed(char key) const;
		bool keyPressed(MouseButtons key) const;

	public:
		bool keyClicked(int key) const;
		bool keyClicked(char key) const;
		bool keyClicked(MouseButtons key) const;

	public:
		bool keyReleased(int key) const;
		bool keyReleased(char key) const;
		bool keyReleased(MouseButtons key) const;

	public:
		void setKeyboardButtonState(int key, bool state);
		void setMouseButtonState(int key, bool state);

	public:
		void calculateCursorPositions(CursorPositionCalculationVariables& v);
		void zeroClickedOrReleasedKeys();

	public:
		void setKeyCode(int vkcode);

	public:
		char getCharTyped() const;
	};
}