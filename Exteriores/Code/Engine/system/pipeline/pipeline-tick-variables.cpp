#include <Engine/system/pipeline/pipeline-tick-variables.h>

namespace Fabr::System
{
	Window*			PTVGlobalContainer::g_activewindow = nullptr;
	Input*			PTVGlobalContainer::g_activeinput = nullptr;
	OpenGLSurface*	PTVGlobalContainer::g_activesurface = nullptr;
	Shader*			PTVGlobalContainer::g_activeshader = nullptr;
	Camera2D*		PTVGlobalContainer::g_activecamera2d = nullptr;
	Camera3D*		PTVGlobalContainer::g_activecamera3d = nullptr;
	float			PTVGlobalContainer::maxFPS = 0.0f;
	float			PTVGlobalContainer::width = 0.0f;
	float			PTVGlobalContainer::height = 0.0f;
	glm::mat4		PTVGlobalContainer::orthographic = glm::mat4(1.0f);
	glm::mat4		PTVGlobalContainer::perspective = glm::mat4(1.0f);

	PipelineTickVariables::PipelineTickVariables()
	{
		window = nullptr;
		input = nullptr;
		surface = nullptr;
		shader = nullptr;
		camera2d = nullptr;
		camera3d = nullptr;
		maxFPS = nullptr;
		width = nullptr;
		height = nullptr;
		orthographic = nullptr;
		perspective = nullptr;
	}

	void PTVGlobalContainer::setPipelineTickVariables(PipelineTickVariables& v)
	{
		g_activewindow = v.window;
		g_activeinput = v.input;
		g_activesurface = v.surface;
		maxFPS = *v.maxFPS;
	}

	void PTVGlobalContainer::setPipelineResizeVariables(PipelineTickVariables& v)
	{
		width = *v.width;
		height = *v.height;
		orthographic = *v.orthographic;
		perspective = *v.perspective;
	}

	void PTVGlobalContainer::setActiveShader(Shader* shader)
	{
		g_activeshader = shader;
	}

	void PTVGlobalContainer::setActiveCamera(Camera2D* camera)
	{
		g_activecamera2d = camera;
	}

	void PTVGlobalContainer::setActiveCamera(Camera3D* camera)
	{
		g_activecamera3d = camera;
	}

	Window* PTVGlobalContainer::getActiveWindow()
	{
		return g_activewindow;
	}

	Input* PTVGlobalContainer::getActiveInput()
	{
		return g_activeinput;
	}

	OpenGLSurface* PTVGlobalContainer::getActiveSurface()
	{
		return g_activesurface;
	}

	Shader* PTVGlobalContainer::getActiveShader()
	{
		return g_activeshader;
	}

	Camera2D* PTVGlobalContainer::getActiveCamera2D()
	{
		return g_activecamera2d;
	}

	Camera3D* PTVGlobalContainer::getActiveCamera3D()
	{
		return g_activecamera3d;
	}

	float PTVGlobalContainer::getMaxFPS()
	{
		return maxFPS;
	}

	float PTVGlobalContainer::getSurfaceWidth()
	{
		return width;
	}

	float PTVGlobalContainer::getSurfaceHeight()
	{
		return height;
	}

	glm::mat4 PTVGlobalContainer::getOrthographicProjection()
	{
		return orthographic;
	}

	glm::mat4 PTVGlobalContainer::getPerspectiveProjection()
	{
		return perspective;
	}

	PipelineTickVariables PTVGlobalContainer::getPipelineTickVariables()
	{
		PipelineTickVariables v;
		v.window = g_activewindow;
		v.input = g_activeinput;
		v.surface = g_activesurface;
		v.shader = g_activeshader;
		v.maxFPS = &maxFPS;
		v.width = &width;
		v.height = &height;
		v.orthographic = &orthographic;
		v.perspective = &perspective;
		return v;
	}
}