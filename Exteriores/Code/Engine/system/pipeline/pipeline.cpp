#include <Engine/system/pipeline/pipeline.h>

namespace Fabr::System
{
	Pipeline::Pipeline()
	{
		should_initialize = 1;
		should_terminate = 0;
		should_change = 0;
	}

	Pipeline::Pipeline(const Pipeline& other)
	{
		should_initialize = other.should_initialize;
		should_terminate = other.should_terminate;
		should_change = other.should_change;
	}

	void Pipeline::onResize(float width, float height)
	{

	}

	bool Pipeline::onInitialize(int eventcode)
	{
		return true;
	}

	bool Pipeline::onTerminate(int eventcode)
	{
		return true;
	}

	bool Pipeline::onChange(int eventcode)
	{
		return true;
	}

	void Pipeline::onLogicTick()
	{

	}

	void Pipeline::onRendererTick()
	{

	}

	void Pipeline::setInitializationEvent(int returncode)
	{
		should_initialize = returncode;
	}
	
	void Pipeline::setTerminationEvent(int returncode)
	{
		should_terminate = returncode;
	}

	void Pipeline::setChangeEvent(int returncode)
	{
		should_change = returncode;
	}

	int Pipeline::shouldInitialize() const
	{
		return should_initialize;
	}

	int Pipeline::shouldTerminate() const
	{
		return should_terminate;
	}

	int Pipeline::shouldChange() const
	{
		return should_change;
	}
}