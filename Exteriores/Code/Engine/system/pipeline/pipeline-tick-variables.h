#pragma once

#include <Engine/system/window/window.h>
#include <Engine/system/input/input.h>
#include <Engine/system/opengl/opengl-surface.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/graphics/camera/camera.h>

namespace Fabr::System
{
	using namespace Fabr::Graphics;

	struct PipelineTickVariables
	{
		Window*			window;
		Input*			input;
		OpenGLSurface*	surface;
		Shader*			shader;
		Camera2D*		camera2d;
		Camera3D*		camera3d;

		float *maxFPS, *width, *height;

		glm::mat4 *orthographic;
		glm::mat4 *perspective;

		PipelineTickVariables();
	};

	class PTVGlobalContainer
	{
	private:
		static Window*			g_activewindow;
		static Input*			g_activeinput;
		static OpenGLSurface*	g_activesurface;
		static Shader*			g_activeshader;
		static Camera2D*		g_activecamera2d;
		static Camera3D*		g_activecamera3d;

	private:
		static float maxFPS, width, height;

	private:
		static glm::mat4 orthographic;
		static glm::mat4 perspective;

	protected:
		PTVGlobalContainer() = delete;
		PTVGlobalContainer(const PTVGlobalContainer& other) = delete;
		~PTVGlobalContainer() = delete;

	public:
		static void setPipelineTickVariables(PipelineTickVariables& v);
		static void setPipelineResizeVariables(PipelineTickVariables& v);
		static void setActiveShader(Shader* shader);
		static void setActiveCamera(Camera2D* camera);
		static void setActiveCamera(Camera3D* camera);

	public:
		static Window* getActiveWindow();
		static Input* getActiveInput();
		static OpenGLSurface* getActiveSurface();
		static Shader* getActiveShader();
		static Camera2D* getActiveCamera2D();
		static Camera3D* getActiveCamera3D();
		static float getMaxFPS();
		static float getSurfaceWidth();
		static float getSurfaceHeight();
		static glm::mat4 getOrthographicProjection();
		static glm::mat4 getPerspectiveProjection();

	public:
		static PipelineTickVariables getPipelineTickVariables();
	};
}