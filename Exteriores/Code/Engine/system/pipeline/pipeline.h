#pragma once

#include <Windows.h>
#include <Engine/faber-object/faber-object.h>

namespace Fabr::System
{
	class Pipeline : public virtual FaberObject
	{
	private:
		int should_initialize;
		int should_terminate;
		int should_change;

	public:
		Pipeline();
		Pipeline(const Pipeline& other);
		virtual ~Pipeline() = default;

	public:
		virtual void onResize(float width, float height);
		virtual bool onInitialize(int eventcode);
		virtual bool onTerminate(int eventcode);
		virtual bool onChange(int eventcode);

	public:
		virtual void onLogicTick();
		virtual void onRendererTick();

	public:
		void setInitializationEvent(int returncode);
		void setTerminationEvent(int returncode);
		void setChangeEvent(int returncode);

	public:
		int shouldInitialize() const;
		int shouldTerminate() const;
		int shouldChange() const;
	};
}