#include <Engine/graphics/texture/texture.h>

#include <string>
#include <Windows.h>
#include <GL/glew.h>
#include <FI/FreeImage.h>

namespace Fabr::Graphics
{
	Texture::Texture()
	{
		id = 0;
		type = TextureType::TEXTURE_TYPE_NONE;
		imagetype = TextureImageType::TEXTURE_IMAGE_NONE;
		surfacetype = TextureSurfaceType::TEXTURE_SURFACE_NONE;
		cubemaptype = TextureCubemapType::TEXTURE_CUBEMAP_NONE;
	}

	Texture::Texture(const Texture& other)
	{
		if (id != 0)
		{
			glDeleteTextures(1, &id);
		}
		id = other.id;
		type = other.type;
		imagetype = other.imagetype;
		surfacetype = other.surfacetype;
		cubemaptype = other.cubemaptype;
	}

	Texture::~Texture()
	{
		if (id != 0)
		{
			glDeleteTextures(1, &id);
			id = 0;
		}
	}

	bool Texture::loadFromFile(const char* path, TextureImageType type, bool generate_mipmap, TextureFiltering minfilter, TextureFiltering magfilter, TextureWrapping s_wrapping, TextureWrapping t_wrapping)
	{
		if (type == TextureImageType::TEXTURE_IMAGE_NONE) return false;

		if (id != 0)
		{
			FWAR("Texture exists.");
			return false;
		}

		this->type = TextureType::TEXTURE_IMAGE;
		imagetype = type;

		FIBITMAP* bitmap = FreeImage_Load(FreeImage_GetFIFFromFilename(path), path, 0);
		if (bitmap == nullptr)
		{
			FERR("Reading file: \"" + std::string(path) + "\" failed.");
			return false;
		}
		FreeImage_FlipVertical(bitmap);

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		switch (imagetype)
		{
		case TextureImageType::TEXTURE_IMAGE:
		case TextureImageType::TEXTURE_IMAGE_HDR:
		{
			FIBITMAP* buffer = FreeImage_ConvertTo32Bits(bitmap);
			if (buffer == nullptr)
			{
				FERR("Reading file: \"" + std::string(path) + "\" failed.");
				FreeImage_Unload(bitmap);
				glBindTexture(GL_TEXTURE_2D, 0);
				glDeleteTextures(1, &id);
				id = 0;
				return false;
			}
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FreeImage_GetWidth(buffer), FreeImage_GetHeight(buffer), 0, GL_BGRA, GL_UNSIGNED_BYTE, FreeImage_GetBits(buffer));
			FreeImage_Unload(buffer);
			break;
		}

		case TextureImageType::TEXTURE_IMAGE_SCALAR:
		case TextureImageType::TEXTURE_IMAGE_SCALAR_HDR:
		{
			FIBITMAP* buffer = FreeImage_ConvertToGreyscale(bitmap);
			if (buffer == nullptr)
			{
				FERR("Reading file: \"" + std::string(path) + "\" failed.");
				FreeImage_Unload(bitmap);
				glBindTexture(GL_TEXTURE_2D, 0);
				glDeleteTextures(1, &id);
				id = 0;
				return false;
			}
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, FreeImage_GetWidth(buffer), FreeImage_GetHeight(buffer), 0, GL_RED, GL_UNSIGNED_BYTE, FreeImage_GetBits(buffer));
			FreeImage_Unload(buffer);
			break;
		}
		}

		FreeImage_Unload(bitmap);

		if (generate_mipmap)
		{
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter == TextureFiltering::LINEAR ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s_wrapping == TextureWrapping::REPEAT ? GL_REPEAT : s_wrapping == TextureWrapping::MIRRORED_REPEAT ? GL_MIRRORED_REPEAT : GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t_wrapping == TextureWrapping::REPEAT ? GL_REPEAT : t_wrapping == TextureWrapping::MIRRORED_REPEAT ? GL_MIRRORED_REPEAT : GL_CLAMP_TO_EDGE);

		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	void Texture::generateSurface(TextureSurfaceType type, unsigned int width, unsigned int height, bool mipmapfilter, TextureFiltering minfilter, TextureFiltering magfilter, TextureWrapping s_wrapping, TextureWrapping t_wrapping)
	{
		if (type == TextureSurfaceType::TEXTURE_SURFACE_NONE) return;

		if (id != 0)
		{
			FWAR("Texture exists.");
			return;
		}

		this->type = TextureType::TEXTURE_SURFACE;
		surfacetype = type;

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		switch (type)
		{
		case TextureSurfaceType::TEXTURE_SURFACE:
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)0);
			break;
		}

		case TextureSurfaceType::TEXTURE_SURFACE_HDR:
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, (void*)0);
			break;
		}

		case TextureSurfaceType::TEXTURE_SURFACE_SCALAR:
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, (void*)0);
			break;
		}

		case TextureSurfaceType::TEXTURE_SURFACE_SCALAR_HDR:
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, (void*)0);
			break;
		}
		}

		if (mipmapfilter)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter == TextureFiltering::LINEAR ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magfilter == TextureFiltering::LINEAR ? GL_LINEAR : GL_NEAREST);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s_wrapping == TextureWrapping::REPEAT ? GL_REPEAT : s_wrapping == TextureWrapping::MIRRORED_REPEAT ? GL_MIRRORED_REPEAT : GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t_wrapping == TextureWrapping::REPEAT ? GL_REPEAT : t_wrapping == TextureWrapping::MIRRORED_REPEAT ? GL_MIRRORED_REPEAT : GL_CLAMP_TO_EDGE);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void Texture::generateMipmap()
	{
		if (id != 0)
		{
			switch (type)
			{
			case TextureType::TEXTURE_SURFACE:
			{
				glBindTexture(GL_TEXTURE_2D, id);
				glGenerateMipmap(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, 0);
				break;
			}
			}
		}
	}

	unsigned int Texture::getID() const
	{
		return id;
	}

	TextureType Texture::getType() const
	{
		return type;
	}

	TextureSurfaceType Texture::getSurfaceType() const
	{
		return surfacetype;
	}
}