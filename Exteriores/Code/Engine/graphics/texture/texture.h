#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>

namespace Fabr::Graphics
{
	enum class TextureType
	{
		TEXTURE_TYPE_NONE = 0,
		TEXTURE_IMAGE, TEXTURE_SURFACE, TEXTURE_CUBEMAP
	};

	enum class TextureImageType
	{
		TEXTURE_IMAGE_NONE = 0,
		TEXTURE_IMAGE, TEXTURE_IMAGE_HDR, TEXTURE_IMAGE_SCALAR, TEXTURE_IMAGE_SCALAR_HDR
	};

	enum class TextureSurfaceType
	{
		TEXTURE_SURFACE_NONE = 0,
		TEXTURE_SURFACE, TEXTURE_SURFACE_HDR, TEXTURE_SURFACE_SCALAR, TEXTURE_SURFACE_SCALAR_HDR
	};

	enum class TextureCubemapType
	{
		TEXTURE_CUBEMAP_NONE = 0,
		TEXTURE_CUBEMAP, TEXTURE_CUBEMAP_HDR, TEXTURE_CUBEMAP_SCALAR, TEXTURE_CUBEMAP_SCALAR_HDR
	};

	enum class TextureFiltering
	{
		LINEAR, NEAREST
	};

	enum class TextureWrapping
	{
		REPEAT, MIRRORED_REPEAT, CLAMP_TO_EDGE
	};

	class Texture : public virtual Fabr::FaberObject
	{
	private:
		unsigned int id;
		TextureType type;

	private:
		TextureImageType imagetype;
		TextureSurfaceType surfacetype;
		TextureCubemapType cubemaptype;

	public:
		Texture();
		Texture(const Texture& other);
		~Texture();

	public:
		bool loadFromFile(const char* path, TextureImageType type, bool generatemipmap = true, TextureFiltering minfilter = TextureFiltering::LINEAR, TextureFiltering magfilter = TextureFiltering::LINEAR, TextureWrapping s_wrapping = TextureWrapping::CLAMP_TO_EDGE, TextureWrapping t_wrapping = TextureWrapping::CLAMP_TO_EDGE);
		//bool loadFromFile(const char* path, TextureCubemapType type);
		//bool loadFromFiles(const char* pathtop, const char* pathbottom, const char* pathleft, const char* pathright, const char* pathfront, const char* pathback, TextureCubemapType type);

	public:
		void generateSurface(TextureSurfaceType type, unsigned int width, unsigned int height, bool mipmapfilter = false, TextureFiltering minfilter = TextureFiltering::LINEAR, TextureFiltering magfilter = TextureFiltering::LINEAR, TextureWrapping s_wrapping = TextureWrapping::CLAMP_TO_EDGE, TextureWrapping t_wrapping = TextureWrapping::CLAMP_TO_EDGE);
		void generateMipmap();

	public:
		unsigned int getID() const;
		TextureType getType() const;

	public:
		TextureSurfaceType getSurfaceType() const;
	};
}