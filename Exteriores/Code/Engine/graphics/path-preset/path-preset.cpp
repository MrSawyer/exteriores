#include <Engine/graphics/path-preset/path-preset.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

#include <Windows.h>
#include <GL/glew.h>

namespace Fabr::Graphics
{
	unsigned int PathPreset::vao = 0, PathPreset::vbo = 0;

	bool PathPreset::generatePreset()
	{
		if (vao != 0 || vbo != 0)
		{
			FWAR("Path preset exists.");
			return false;
		}

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		return true;
	}

	void PathPreset::terminatePreset()
	{
		if (vbo != 0)
		{
			glDeleteBuffers(1, &vbo);
			vbo = 0;
		}

		if (vao != 0)
		{
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
	}

	void PathPreset::draw(std::vector<glm::vec3>& vertices, glm::vec4 color, float width)
	{
		Shader* activeshader = System::PTVGlobalContainer::getActiveShader();
		if (activeshader != nullptr)
		{
			std::vector <glm::vec3> triangles;

			Camera3D* activecamera = System::PTVGlobalContainer::getActiveCamera3D();
			if (activecamera != nullptr)
			{
				glm::mat4 MVP = System::PTVGlobalContainer::getPerspectiveProjection() * activecamera->getMatrix();
				activeshader->send(MVP, "MVP");

				for (size_t i = 0; i < vertices.size() - 1; i += 2)
				{
					glm::vec3 A = vertices[i];
					glm::vec3 B = vertices[i + 1];
					glm::vec3 C = activecamera->getPosition() - activecamera->getTarget();
					glm::vec3 S = glm::normalize(glm::cross(B - A, C));

					glm::vec3 P1 = A + width / 2.0f * S; triangles.push_back(P1);
					glm::vec3 P2 = B + width / 2.0f * S; triangles.push_back(P2);
					glm::vec3 P3 = A - width / 2.0f * S; triangles.push_back(P3);
					glm::vec3 P4 = A - width / 2.0f * S; triangles.push_back(P4);
					glm::vec3 P5 = B - width / 2.0f * S; triangles.push_back(P5);
					glm::vec3 P6 = B + width / 2.0f * S; triangles.push_back(P6);
				}
			}
			else
			{
				glm::mat4 MVP = System::PTVGlobalContainer::getPerspectiveProjection();
				activeshader->send(MVP, "MVP");

				for (size_t i = 0; i < vertices.size() - 1; i += 2)
				{
					glm::vec3 A = vertices[i];
					glm::vec3 B = vertices[i + 1];
					glm::vec3 C = glm::vec3(0.0f, 1.0f, 0.0f);
					glm::vec3 S = glm::normalize(glm::cross(B - A, C));

					glm::vec3 P1 = A + width / 2.0f * S; triangles.push_back(P1);
					glm::vec3 P2 = B + width / 2.0f * S; triangles.push_back(P2);
					glm::vec3 P3 = A - width / 2.0f * S; triangles.push_back(P3);
					glm::vec3 P4 = A - width / 2.0f * S; triangles.push_back(P4);
					glm::vec3 P5 = B - width / 2.0f * S; triangles.push_back(P5);
					glm::vec3 P6 = B + width / 2.0f * S; triangles.push_back(P6);
				}
			}

			activeshader->send(color, "u_color");

			glBindVertexArray(vao);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, triangles.size() * sizeof(glm::vec3), (void*)&triangles[0], GL_DYNAMIC_DRAW);
			glDrawArrays(GL_TRIANGLES, 0, (GLsizei)triangles.size());
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
	}
}