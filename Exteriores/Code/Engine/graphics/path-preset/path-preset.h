#pragma once

#include <Engine/flog/flog.h>
#include <vector>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

namespace Fabr::Graphics
{
	class PathPreset
	{
	private:
		static unsigned int vao, vbo;

	public:
		PathPreset() = delete;
		PathPreset(const PathPreset& other) = delete;
		~PathPreset() = delete;

	public:
		static bool generatePreset();
		static void terminatePreset();

	public:
		static void draw(std::vector<glm::vec3>& vertices, glm::vec4 color, float width);
	};
}