#include <Engine/graphics/shader/shader.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include <Windows.h>
#include <GL/glew.h>

namespace Fabr::Graphics
{
	Shader::Shader()
	{
		id = 0;
	}

	Shader::Shader(const Shader& other)
	{
		if (id != 0)
		{
			glDeleteProgram(id);
		}
		id = other.id;
	}

	Shader::~Shader()
	{
		if (id != 0)
		{
			glDeleteProgram(id);
			id = 0;
		}
	}

	bool Shader::includeFile(const char* path, std::string& target, size_t begin, size_t end)
	{
		std::string		code;
		std::ifstream	file;

		file.exceptions(std::ifstream::badbit);

		try
		{
			std::stringstream stream;

			file.open(path);
			stream << file.rdbuf();
			file.close();

			code = stream.str();
		}
		catch (std::ifstream::failure)
		{
			FERR("Reading file: \"" + std::string(path) + "\" failed.");
			return false;
		}

		target.replace(begin, end - begin, code);

		return true;
	}

	bool Shader::iterateIncludes(const char* directory, std::string& target)
	{
		size_t begin = target.find("#include");
		while (begin != std::string::npos)
		{
			size_t end = target.find_first_of('\n', begin);
			std::string include = target.substr(begin, end - begin);

			size_t b = include.find_first_of('"') + 1;
			size_t e = include.find_last_of('"');
			std::string path = std::string(directory) + include.substr(b, e - b);

			if (!includeFile(path.c_str(), target, begin, end))
			{
				return false;
			}

			begin = target.find("#include");
		}

		return true;
	}

	bool Shader::loadFromFiles(const char* vs_path, const char* fs_path)
	{
		if (id != 0)
		{
			FWAR("Shader exists.");
			return false;
		}

		std::string		vs_code, fs_code;
		std::ifstream	vs_file, fs_file;

		vs_file.exceptions(std::ifstream::badbit);
		fs_file.exceptions(std::ifstream::badbit);

		try
		{
			std::stringstream vs_stream, fs_stream;

			vs_file.open(vs_path);
			vs_stream << vs_file.rdbuf();
			vs_file.close();

			fs_file.open(fs_path);
			fs_stream << fs_file.rdbuf();
			fs_file.close();

			vs_code = vs_stream.str();
			fs_code = fs_stream.str();
		}
		catch (std::ifstream::failure)
		{
			FERR("Reading file: \"" + std::string(vs_path) + "\" failed.");
			return false;
		}

		std::string vs_directory = std::string(vs_path); vs_directory = vs_directory.substr(0, vs_directory.find_last_of('/') + 1);
		std::string fs_directory = std::string(fs_path); fs_directory = fs_directory.substr(0, fs_directory.find_last_of('/') + 1);

		if (!iterateIncludes(vs_directory.c_str(), vs_code)) return false;
		if (!iterateIncludes(fs_directory.c_str(), fs_code)) return false;

		const GLchar* gl_vs_code = vs_code.c_str();
		const GLchar* gl_fs_code = fs_code.c_str();

		GLuint	vs_id, fs_id;
		GLchar	errors_log[1024];
		GLint	result;

		vs_id = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs_id, 1, &gl_vs_code, nullptr);
		glCompileShader(vs_id);
		glGetShaderiv(vs_id, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(vs_id, 1024, nullptr, errors_log);

			FERR("B��d \"Vertex shader\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);

			return false;
		}

		fs_id = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fs_id, 1, &gl_fs_code, nullptr);
		glCompileShader(fs_id);
		glGetShaderiv(fs_id, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(fs_id, 1024, nullptr, errors_log);

			FERR("B��d \"Fragment shader\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);
			glDeleteShader(fs_id);

			return false;
		}

		id = glCreateProgram();
		glAttachShader(id, vs_id);
		glAttachShader(id, fs_id);
		glLinkProgram(id);
		glGetProgramiv(id, GL_LINK_STATUS, &result);
		if (!result)
		{
			glGetProgramInfoLog(id, 1024, nullptr, errors_log);

			FERR("B��d \"Program linking\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);
			glDeleteShader(fs_id);
			glDeleteProgram(id);
			id = 0;

			return false;
		}

		glDeleteShader(vs_id);
		glDeleteShader(fs_id);

		return true;
	}

	bool Shader::loadFromMemory(std::string vs_code, std::string fs_code)
	{
		if (id != 0)
		{
			FWAR("Shader exists.");
			return false;
		}

		if (!iterateIncludes("", vs_code)) return false;
		if (!iterateIncludes("", fs_code)) return false;

		const GLchar* gl_vs_code = vs_code.c_str();
		const GLchar* gl_fs_code = fs_code.c_str();

		GLuint	vs_id, fs_id;
		GLchar	errors_log[1024];
		GLint	result;

		vs_id = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs_id, 1, &gl_vs_code, nullptr);
		glCompileShader(vs_id);
		glGetShaderiv(vs_id, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(vs_id, 1024, nullptr, errors_log);

			FERR("B��d \"Vertex shader\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);

			return false;
		}

		fs_id = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fs_id, 1, &gl_fs_code, nullptr);
		glCompileShader(fs_id);
		glGetShaderiv(fs_id, GL_COMPILE_STATUS, &result);
		if (!result)
		{
			glGetShaderInfoLog(fs_id, 1024, nullptr, errors_log);

			FERR("B��d \"Fragment shader\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);
			glDeleteShader(fs_id);

			return false;
		}

		id = glCreateProgram();
		glAttachShader(id, vs_id);
		glAttachShader(id, fs_id);
		glLinkProgram(id);
		glGetProgramiv(id, GL_LINK_STATUS, &result);
		if (!result)
		{
			glGetProgramInfoLog(id, 1024, nullptr, errors_log);

			FERR("B��d \"Program linking\":");
			std::cout << errors_log << std::endl << std::endl;

			glDeleteShader(vs_id);
			glDeleteShader(fs_id);
			glDeleteProgram(id);
			id = 0;

			return false;
		}

		glDeleteShader(vs_id);
		glDeleteShader(fs_id);

		return true;
	}

	void Shader::terminateShader()
	{
		if (id != 0)
		{
			glDeleteProgram(id);
			id = 0;
		}
	}

	void Shader::use()
	{
		glUseProgram(id);
		System::PTVGlobalContainer::setActiveShader(this);
	}

	void Shader::send(int value, const char* target)
	{
		glUniform1i(glGetUniformLocation(id, target), value);
	}

	void Shader::send(float value, const char* target)
	{
		glUniform1f(glGetUniformLocation(id, target), value);
	}

	void Shader::send(glm::vec2 value, const char* target)
	{
		glUniform2fv(glGetUniformLocation(id, target), 1, glm::value_ptr(value));
	}

	void Shader::send(glm::vec3 value, const char* target)
	{
		glUniform3fv(glGetUniformLocation(id, target), 1, glm::value_ptr(value));
	}

	void Shader::send(glm::vec4 value, const char* target)
	{
		glUniform4fv(glGetUniformLocation(id, target), 1, glm::value_ptr(value));
	}

	void Shader::send(glm::mat3 value, const char* target)
	{
		glUniformMatrix3fv(glGetUniformLocation(id, target), 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::send(glm::mat4 value, const char* target)
	{
		glUniformMatrix4fv(glGetUniformLocation(id, target), 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::send(Texture* texture, const char* target, int slot)
	{
		if (texture != nullptr)
		{
			glActiveTexture(GL_TEXTURE0 + slot);
			glUniform1i(glGetUniformLocation(id, target), slot);
			if (texture->getType() == TextureType::TEXTURE_CUBEMAP)
			{
				glBindTexture(GL_TEXTURE_CUBE_MAP, texture->getID());
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, texture->getID());
			}
		}
	}

	void Shader::send(PointLight* light, int slot)
	{
		if (light != nullptr)
		{
			glUniform3fv(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].position").c_str()), 1, glm::value_ptr(light->getPosition()));
			glUniform3fv(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].color").c_str()), 1, glm::value_ptr(light->getColorRGB()));
			glUniform1f(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].power").c_str()), light->getPower());
			glUniform1f(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].attenuation_constant").c_str()), light->getAttenuationConstant());
			glUniform1f(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].attenuation_linear").c_str()), light->getAttenuationLinear());
			glUniform1f(glGetUniformLocation(id, std::string("u_pointlights[" + std::to_string(slot) + "].attenuation_quadratic").c_str()), light->getAttenuationQuadratic());
		}
	}

	void Shader::send(SpotLight* light, int slot)
	{
		if (light != nullptr)
		{
			glUniform3fv(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].position").c_str()), 1, glm::value_ptr(light->getPosition()));
			glUniform3fv(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].color").c_str()), 1, glm::value_ptr(light->getColorRGB()));
			glUniform3fv(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].direction").c_str()), 1, glm::value_ptr(light->getDirection()));
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].power").c_str()), light->getPower());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].attenuation_constant").c_str()), light->getAttenuationConstant());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].attenuation_linear").c_str()), light->getAttenuationLinear());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].attenuation_quadratic").c_str()), light->getAttenuationQuadratic());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].phi").c_str()), light->getPhi());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].theta").c_str()), light->getTheta());
			glUniform1f(glGetUniformLocation(id, std::string("u_spotlights[" + std::to_string(slot) + "].gamma").c_str()), light->getGamma());
		}
	}
}