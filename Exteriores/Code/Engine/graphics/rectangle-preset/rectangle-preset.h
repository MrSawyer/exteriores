#pragma once

#include <Engine/flog/flog.h>

namespace Fabr::Graphics
{
	class RectanglePreset
	{
	private:
		static unsigned int vao, vbo;

	protected:
		RectanglePreset() = delete;
		RectanglePreset(const RectanglePreset& other) = delete;
		~RectanglePreset() = delete;

	public:
		static bool generatePreset();
		static void terminatePreset();

	public:
		static void draw();
	};
}