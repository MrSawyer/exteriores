#include <Engine/graphics/rectangle-preset/rectangle-preset.h>

#include <vector>
#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>

namespace Fabr::Graphics
{
	unsigned int RectanglePreset::vao = 0, RectanglePreset::vbo = 0;

	bool RectanglePreset::generatePreset()
	{
		if (vao != 0 || vbo != 0)
		{
			FWAR("Rectangle preset exists.");
			return false;
		}

		std::vector <glm::vec2> positions;
		std::vector <glm::vec2> uvs;

		positions.push_back(glm::vec2(-0.5f, -0.5f));
		positions.push_back(glm::vec2(-0.5f, 0.5f));
		positions.push_back(glm::vec2(0.5f, -0.5f));
		positions.push_back(glm::vec2(0.5f, 0.5f));

		uvs.push_back(glm::vec2(0.0f, 1.0f));
		uvs.push_back(glm::vec2(0.0f, 0.0f));
		uvs.push_back(glm::vec2(1.0f, 1.0f));
		uvs.push_back(glm::vec2(1.0f, 0.0f));

		const size_t positions_size = positions.size() * sizeof(glm::vec2);
		const size_t uvs_size = uvs.size() * sizeof(glm::vec2);

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, positions_size + uvs_size, nullptr, GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, positions_size, (void*)&positions[0]);
		glBufferSubData(GL_ARRAY_BUFFER, positions_size, uvs_size, (void*)&uvs[0]);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(positions_size));

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		return true;
	}

	void RectanglePreset::terminatePreset()
	{
		if (vbo != 0)
		{
			glDeleteBuffers(1, &vbo);
			vbo = 0;
		}

		if (vao != 0)
		{
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
	}

	void RectanglePreset::draw()
	{
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glBindVertexArray(0);
	}
}