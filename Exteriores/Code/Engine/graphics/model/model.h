#pragma once

#include <Engine/flog/flog.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/graphics/texture/texture.h>
#include <Engine/graphics/rectangle-preset/rectangle-preset.h>
#include <Engine/transformable/transformable.h>
#include <Engine/colorable/colorable.h>

#include <vector>
#include <memory>
#include <GLM/glm.hpp>

namespace Fabr::Graphics
{
	struct VBODataSet
	{
		std::vector<glm::vec3>		positions;
		std::vector<glm::vec3>		normals;
		std::vector<glm::vec2>		uvs;
		std::vector<unsigned int>	ids;
		bool						success;

		VBODataSet();
	};

	class Model : public virtual Fabr::Math::Colorable, public Fabr::Math::Transformable
	{
	private:
		unsigned int vao, vbo, ebo;
		unsigned int vertices_count;

	protected:
		VBODataSet loadVBODataSetFromFile(const char* path);

	private:
		Texture* texture;
		std::shared_ptr<Texture> owned_texture;

	public:
		Model();
		Model(const Model& other);
		~Model();

	public:
		bool loadFromFile(const char* path);

	public:
		void draw();

	public:
		void setTexture(Texture* texture);

	public:
		Texture* getTexture() const;
	};
}