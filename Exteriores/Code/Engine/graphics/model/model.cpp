#include <Engine/graphics/model/model.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

#include <Windows.h>
#include <GL/glew.h>

#include <ASSIMP/Importer.hpp>
#include <ASSIMP/scene.h>
#include <ASSIMP/postprocess.h>

namespace Fabr::Graphics
{
	VBODataSet::VBODataSet()
	{
		success = false;
	}

	glm::vec3 aiVector3D_To_glmVec3(aiVector3D x)
	{
		glm::vec3 y;
		y.x = x.x;
		y.y = x.y;
		y.z = x.z;
		return y;
	}

	glm::vec2 aiVector3D_To_glmVec2(aiVector3D x)
	{
		glm::vec2 y;
		y.x = x.x;
		y.y = x.y;
		return y;
	}

	VBODataSet Model::loadVBODataSetFromFile(const char* path)
	{
		VBODataSet result;

		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcessPreset_TargetRealtime_Fast | aiProcess_TransformUVCoords | aiProcess_FlipUVs);
		if (scene == nullptr)
		{
			FERR("Reading file: \"" + std::string(path) + "\" failed.");
			return result;
		}

		for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
		{
			aiMesh* mesh = scene->mMeshes[i];

			for (unsigned int j = 0; j < mesh->mNumVertices; ++j)
			{
				if (mesh->HasPositions())
				{
					result.positions.push_back(aiVector3D_To_glmVec3(mesh->mVertices[j]));
				}
				else
				{
					result.positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
				}

				if (mesh->HasNormals())
				{
					result.normals.push_back(aiVector3D_To_glmVec3(mesh->mNormals[j]));
				}
				else
				{
					result.normals.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
				}

				if (mesh->HasTextureCoords(0))
				{
					result.uvs.push_back(aiVector3D_To_glmVec2(mesh->mTextureCoords[0][j]));
				}
				else
				{
					result.uvs.push_back(glm::vec2(0.0f, 0.0f));
				}
			}

			for (unsigned int j = 0; j < mesh->mNumFaces; ++j)
			{
				for (unsigned int k = 0; k < mesh->mFaces[j].mNumIndices; ++k)
				{
					unsigned int id = (unsigned int)(mesh->mFaces[j].mIndices[k] + result.positions.size() - mesh->mNumVertices);
					result.ids.push_back(id);
				}
			}

			if (owned_texture == nullptr)
			{
				aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

				unsigned int textures_count = material->GetTextureCount(aiTextureType_DIFFUSE);
				if (textures_count > 1)
				{
					FERR("Reading file: \"" + std::string(path) + "\" failed. TEX_COUNT > 1");
					return result;
				}
				
				aiString aipath;
				if (material->Get(AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), aipath) == AI_SUCCESS)
				{
					owned_texture = std::make_shared<Texture>();

					std::string filepath = std::string(path);
					if (!owned_texture->loadFromFile(std::string(filepath.substr(0, filepath.find_last_of('/') + 1) + std::string(aipath.C_Str())).c_str(), TextureImageType::TEXTURE_IMAGE))
					{
						owned_texture.reset();
						return result;
					}
					texture = owned_texture.get();
				}
			}
		}

		result.success = true;
		return result;
	}

	Model::Model()
	{
		vao = 0;
		vbo = 0;
		ebo = 0;
		vertices_count = 0;
		texture = nullptr;
	}

	Model::Model(const Model& other)
	{
		if (ebo != 0)
		{
			glDeleteBuffers(1, &ebo);
		}

		if (vbo != 0)
		{
			glDeleteBuffers(1, &vbo);
		}

		if (vao != 0)
		{
			glDeleteVertexArrays(1, &vao);
		}

		vao = other.vao;
		vbo = other.vbo;
		ebo = other.ebo;
		vertices_count = other.vertices_count;
		texture = other.texture;
	}

	Model::~Model()
	{
		if (ebo != 0)
		{
			glDeleteBuffers(1, &ebo);
			ebo = 0;
		}

		if (vbo != 0)
		{
			glDeleteBuffers(1, &vbo);
			vbo = 0;
		}

		if (vao != 0)
		{
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
	}

	bool Model::loadFromFile(const char* path)
	{
		if (vao != 0 || vbo != 0 || ebo != 0 || vertices_count != 0)
		{
			FWAR("Model exists.");
			return false;
		}

		VBODataSet data = loadVBODataSetFromFile(path);
		if (!data.success)
		{
			FERR("Reading file: \"" + std::string(path) + "\" failed.");
			return false;
		}

		const size_t positions_size = data.positions.size() * sizeof(glm::vec3);
		const size_t normals_size = data.normals.size() * sizeof(glm::vec3);
		const size_t uvs_size = data.uvs.size() * sizeof(glm::vec2);
		const size_t ids_size = data.ids.size() * sizeof(unsigned int);

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, positions_size + normals_size + uvs_size, nullptr, GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, positions_size, (void*)&data.positions[0]);
		glBufferSubData(GL_ARRAY_BUFFER, positions_size, normals_size, (void*)&data.normals[0]);
		glBufferSubData(GL_ARRAY_BUFFER, positions_size + normals_size, uvs_size, (void*)&data.uvs[0]);

		glGenBuffers(1, &ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ids_size, (void*)&data.ids[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)(positions_size));

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(positions_size + normals_size));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		vertices_count = (unsigned int)(data.ids.size());

		return true;
	}

	void Model::draw()
	{
		Shader* activeshader = System::PTVGlobalContainer::getActiveShader();
		if (activeshader != nullptr)
		{
			Camera3D* activecamera = System::PTVGlobalContainer::getActiveCamera3D();
			if (activecamera != nullptr)
			{
				glm::mat4 M = getMatrix();
				glm::mat4 MVP = System::PTVGlobalContainer::getPerspectiveProjection() * activecamera->getMatrix() * M;
				activeshader->send(MVP, "MVP");
				activeshader->send(M, "M");
			}
			else
			{
				glm::mat4 M = getMatrix();
				glm::mat4 MVP = System::PTVGlobalContainer::getPerspectiveProjection() * M;
				activeshader->send(MVP, "MVP");
				activeshader->send(M, "M");
			}

			activeshader->send(getColorRGBA(), "u_material.color");

			if (texture != nullptr)
			{
				activeshader->send(texture, "u_material.image", 0);
				activeshader->send(1, "u_material.hasimage");
			}
			else
			{
				activeshader->send(0, "u_material.hasimage");
			}

			glBindVertexArray(vao);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
			glDrawElements(GL_TRIANGLES, vertices_count, GL_UNSIGNED_INT, (void*)0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
		else
		{
			FWAR("Active shader is null.");
		}
	}

	void Model::setTexture(Texture* texture)
	{
		if (owned_texture != nullptr)
		{
			owned_texture.reset();
		}

		this->texture = texture;
	}

	Texture* Model::getTexture() const
	{
		return texture;
	}
}