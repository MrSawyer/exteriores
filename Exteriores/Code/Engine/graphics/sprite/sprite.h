#pragma once

#include <Engine/flog/flog.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/graphics/texture/texture.h>
#include <Engine/graphics/rectangle-preset/rectangle-preset.h>
#include <Engine/transformable/transformable.h>
#include <Engine/colorable/colorable.h>

namespace Fabr::Graphics
{
	class Sprite : public Fabr::Math::Colorable, public virtual Fabr::Math::Transformable
	{
	private:
		Texture* texture;

	public:
		Sprite();
		Sprite(const Sprite& other);
		~Sprite();

	public:
		void draw();

	public:
		void setTexture(Texture* texture);

	public:
		Texture* getTexture() const;
	};
}