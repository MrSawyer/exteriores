#include <Engine/graphics/sprite/sprite.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

namespace Fabr::Graphics
{
	Sprite::Sprite()
	{
		texture = nullptr;
	}

	Sprite::Sprite(const Sprite& other)
	{
		texture = other.texture;
	}

	Sprite::~Sprite() {}

	void Sprite::draw()
	{
		Shader* activeshader = System::PTVGlobalContainer::getActiveShader();
		if (activeshader != nullptr)
		{
			Camera2D* activecamera = System::PTVGlobalContainer::getActiveCamera2D();
			if (activecamera != nullptr)
			{
				glm::mat4 MVP = System::PTVGlobalContainer::getOrthographicProjection() * glm::inverse(activecamera->getMatrix()) * getMatrix();
				activeshader->send(MVP, "MVP");
			}
			else
			{
				glm::mat4 MVP = System::PTVGlobalContainer::getOrthographicProjection() * getMatrix();
				activeshader->send(MVP, "MVP");
			}

			glm::vec4 color = getColorRGBA();
			activeshader->send(color, "color");

			if (texture != nullptr)
			{
				activeshader->send(texture, "image", 0);
				activeshader->send(1, "hasimage");
			}
			else
			{
				activeshader->send(0, "hasimage");
			}

			RectanglePreset::draw();
		}
		else
		{
			FWAR("Active shader is null.");
		}
	}

	void Sprite::setTexture(Texture* texture)
	{
		this->texture = texture;
	}

	Texture* Sprite::getTexture() const
	{
		return texture;
	}
}