#pragma once

#include <Engine/flog/flog.h>

namespace Fabr::Graphics
{
	class CharacterPreset
	{
	private:
		static unsigned int vao, vbo;

	protected:
		CharacterPreset() = delete;
		CharacterPreset(const CharacterPreset& other) = delete;
		~CharacterPreset() = delete;

	public:
		static bool generatePreset();
		static void terminatePreset();

	public:
		static const unsigned int getVAO();
		static const unsigned int getVBO();
	};
}