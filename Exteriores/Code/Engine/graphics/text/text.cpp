#include <Engine/graphics/text/text.h>
#include <Engine/graphics/text/character-preset.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

#include <Windows.h>
#include <GL/glew.h>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

namespace Fabr::Graphics
{
	Text::Text()
	{
		font = nullptr;
	}

	Text::Text(const Text& other)
	{
		font = other.font;
		text = other.text;
	}

	Text::~Text() {}

	void Text::draw()
	{
		if (font != nullptr)
		{
			Shader* activeshader = System::PTVGlobalContainer::getActiveShader();
			if (activeshader != nullptr)
			{
				Camera2D* activecamera = System::PTVGlobalContainer::getActiveCamera2D();
				if (activecamera != nullptr)
				{
					glm::mat4 MVP = System::PTVGlobalContainer::getOrthographicProjection() * glm::inverse(activecamera->getMatrix()) * getMatrix();
					activeshader->send(MVP, "MVP");
				}
				else
				{
					glm::mat4 MVP = System::PTVGlobalContainer::getOrthographicProjection() * getMatrix();
					activeshader->send(MVP, "MVP");
				}

				activeshader->send(getColorRGBA(), "u_color");

				glBindVertexArray(CharacterPreset::getVAO());
				glBindBuffer(GL_ARRAY_BUFFER, CharacterPreset::getVBO());
				glActiveTexture(GL_TEXTURE0);

				float x = 0.0f, y = 0.0f;
				for (unsigned int i = 0; i < text.length(); ++i)
				{
					if (text[i] == '\n')
					{
						x = 0.0f;
						y -= font->getHeight() + 4;
					}
					else
					{
						Character ch = font->getCharacter(text[i]);

						const glm::vec2 p = glm::vec2(x + ch.bearing.x, y - (ch.size.y - ch.bearing.y));
						const glm::vec2 s = glm::vec2(ch.size.x, ch.size.y);

						const float vertices[6][4] =
						{
							{ p.x,			p.y + s.y,	0.0f, 0.0f },
							{ p.x,			p.y,		0.0f, 1.0f },
							{ p.x + s.x,	p.y,		1.0f, 1.0f },
							{ p.x,			p.y + s.y,	0.0f, 0.0f },
							{ p.x + s.x,	p.y,		1.0f, 1.0f },
							{ p.x + s.x,	p.y + s.y,	1.0f, 0.0f }
						};

						glBufferSubData(GL_ARRAY_BUFFER, 0, 96, vertices);

						x += (ch.advance >> 6);

						glBindTexture(GL_TEXTURE_2D, ch.id);
						glDrawArrays(GL_TRIANGLES, 0, 6);
					}
				}

				glBindTexture(GL_TEXTURE_2D, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			}
			else
			{
				FWAR("Active shader is null.");
			}
		}
		else
		{
			FWAR("Font is null.");
		}
	}

	void Text::setFont(Font* font)
	{
		this->font = font;
	}

	void Text::setText(std::string text)
	{
		this->text = text;
	}

	void Text::appendText(std::string text)
	{
		this->text += text;
	}

	void Text::removeLastChar()
	{
		if (text.empty()) return;
		text.pop_back();
	}

	Font* Text::getFont() const
	{
		return font;
	}

	std::string Text::getText() const
	{
		return text;
	}
}