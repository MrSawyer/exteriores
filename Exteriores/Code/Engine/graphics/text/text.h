#pragma once

#include <Engine/graphics/text/font.h>
#include <Engine/colorable/colorable.h>
#include <Engine/transformable/transformable.h>

namespace Fabr::Graphics
{
	class Text : public Fabr::Math::Colorable, public virtual Fabr::Math::Transformable
	{
	private:
		Font* font;
		std::string text;

	public:
		Text();
		Text(const Text& other);
		~Text();

	public:
		void draw();

	public:
		void setFont(Font* font);
		void setText(std::string text);
		void appendText(std::string text);
		void removeLastChar();

	public:
		Font*		getFont() const;
		std::string	getText() const;
	};
}