#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <GLM/glm.hpp>
#include <string>
#include <map>

namespace Fabr::Graphics
{
	struct Character
	{
		glm::ivec2		size;
		glm::ivec2		bearing;
		signed long		advance = 0;
		unsigned int	id = 0;
	};

	class Font : public virtual Fabr::FaberObject
	{
	private:
		std::map <char, Character> characters;
		int height;

	public:
		Font();
		Font(const Font& other);
		~Font();

	public:
		bool loadFromFile(const char* path, int height = 48);

	public:
		const Character& getCharacter(char character) const;
		const int getHeight() const;
	};
}