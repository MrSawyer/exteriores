#include <Engine/graphics/text/font.h>

#include <Windows.h>
#include <GL/glew.h>
#include <FT/ft2build.h>
#include FT_FREETYPE_H

namespace Fabr::Graphics
{
	Font::Font()
	{
		height = 0;
	}

	Font::Font(const Font& other)
	{
		if (!characters.empty())
		{
			for (auto& c : characters)
			{
				if (c.second.id != 0)
				{
					glDeleteTextures(1, &c.second.id);
					c.second.id = 0;
				}
			}
			characters.clear();
		}
		characters = other.characters;
		height = 0;
	}

	Font::~Font()
	{
		for (auto& c : characters)
		{
			if (c.second.id != 0)
			{
				glDeleteTextures(1, &c.second.id);
				c.second.id = 0;
			}
		}
		characters.clear();
	}

	bool Font::loadFromFile(const char* path, int height)
	{
		FT_Library ft;
		if (FT_Init_FreeType(&ft))
		{
			FERR("FreeType initialization failed.");
			return false;
		}

		FT_Face face;
		if (FT_New_Face(ft, path, 0, &face))
		{
			FERR("Reading file: \"" + std::string(path) + "\" failed.");
			FT_Done_FreeType(ft);
			return false;
		}

		FT_Set_Pixel_Sizes(face, 0, height);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		for (unsigned char c = 0; c < 128; ++c)
		{
			if (FT_Load_Char(face, c, FT_LOAD_RENDER))
			{
				FWAR("Loading character \"" + std::to_string((unsigned int)c) + ": " + std::to_string(c) + "\" failed.");
				continue;
			}

			unsigned int id;
			glGenTextures(1, &id);
			glBindTexture(GL_TEXTURE_2D, id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glBindTexture(GL_TEXTURE_2D, 0);
			
			Character character =
			{
				glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
				glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
				face->glyph->advance.x,
				id
			};

			characters.emplace(c, character);
		}

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		FT_Done_Face(face);
		FT_Done_FreeType(ft);

		this->height = height;

		return true;
	}

	const Character& Font::getCharacter(char character) const
	{
		auto result = characters.find(character);
		return result->second;
	}

	const int Font::getHeight() const
	{
		return height;
	}
}