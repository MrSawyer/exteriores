#include <Engine/graphics/text/character-preset.h>

#include <vector>
#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>

namespace Fabr::Graphics
{
	unsigned int CharacterPreset::vao = 0, CharacterPreset::vbo = 0;

	bool CharacterPreset::generatePreset()
	{
		if (vao != 0 || vbo != 0)
		{
			FWAR("Character preset exists.");
			return false;
		}

		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(glm::vec4), nullptr, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		return true;
	}

	void CharacterPreset::terminatePreset()
	{
		if (vbo != 0)
		{
			glDeleteBuffers(1, &vbo);
			vbo = 0;
		}

		if (vao != 0)
		{
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
	}

	const unsigned int CharacterPreset::getVAO()
	{
		return vao;
	}

	const unsigned int CharacterPreset::getVBO()
	{
		return vbo;
	}
}