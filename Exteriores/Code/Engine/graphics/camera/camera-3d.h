#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>

namespace Fabr::Graphics
{
	class Camera3D : public virtual FaberObject
	{
	private:
		bool should_update;

	private:
		glm::vec3 position;
		glm::vec3 target;
		glm::vec3 up;

	private:
		glm::mat4 V;

	protected:
		void update();

	public:
		Camera3D();
		Camera3D(const Camera3D& other);
		~Camera3D();
			
	public:
		glm::mat4 getMatrix();

	public:
		void use();

	public:
		void setPosition(glm::vec3 position);
		void setPosition(float x, float y, float z);
		void setPositionX(float x);
		void setPositionY(float y);
		void setPositionZ(float z);

	public:
		void setTarget(glm::vec3 target);
		void setTarget(float x, float y, float z);
		void setTargetX(float x);
		void setTargetY(float y);
		void setTargetZ(float z);

	public:
		void setUp(glm::vec3 up);
		void setUp(float x, float y, float z);
		void setUpX(float x);
		void setUpY(float y);
		void setUpZ(float z);

	public:
		glm::vec3 getPosition() const;
		float getPositionX() const;
		float getPositionY() const;
		float getPositionZ() const;

	public:
		glm::vec3 getTarget() const;
		float getTargetX() const;
		float getTargetY() const;
		float getTargetZ() const;

	public:
		glm::vec3 getUp() const;
		float getUpX() const;
		float getUpY() const;
		float getUpZ() const;
	};
}