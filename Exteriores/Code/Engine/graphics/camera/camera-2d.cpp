#include <Engine/graphics/camera/camera-2d.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

namespace Fabr::Graphics
{
	Camera2D::Camera2D() {}

	Camera2D::Camera2D(const Camera2D& other) {}

	Camera2D::~Camera2D() {}

	void Camera2D::use()
	{
		System::PTVGlobalContainer::setActiveCamera(this);
	}
}