#include <Engine/graphics/camera/camera-3d.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

#include <GLM/gtc/matrix_transform.hpp>

namespace Fabr::Graphics
{
	Camera3D::Camera3D()
	{
		position = glm::vec3(0.0f, 0.0f, 3.0f);
		target = glm::vec3(0.0f, 0.0f, 0.0f);
		up = glm::vec3(0.0f, 1.0f, 0.0f);
		should_update = true;
	}

	Camera3D::Camera3D(const Camera3D& other)
	{
		position = other.position;
		target = other.target;
		up = other.up;
		should_update = true;
	}

	Camera3D::~Camera3D() {}

	void Camera3D::update()
	{
		if (should_update)
		{
			V = glm::lookAt(position, target, up);
			should_update = false;
		}
	}

	glm::mat4 Camera3D::getMatrix()
	{
		update();
		return V;
	}

	void Camera3D::use()
	{
		System::PTVGlobalContainer::setActiveCamera(this);
	}

	void Camera3D::setPosition(glm::vec3 position)
	{
		this->position = position;
		should_update = true;
	}

	void Camera3D::setPosition(float x, float y, float z)
	{
		position.x = x;
		position.y = y;
		position.z = z;
		should_update = true;
	}

	void Camera3D::setPositionX(float x)
	{
		position.x = x;
		should_update = true;
	}

	void Camera3D::setPositionY(float y)
	{
		position.y = y;
		should_update = true;
	}

	void Camera3D::setPositionZ(float z)
	{
		position.z = z;
		should_update = true;
	}

	void Camera3D::setTarget(glm::vec3 target)
	{
		this->target = target;
		should_update = true;
	}

	void Camera3D::setTarget(float x, float y, float z)
	{
		target.x = x;
		target.y = y;
		target.z = z;
		should_update = true;
	}

	void Camera3D::setTargetX(float x)
	{
		target.x = x;
		should_update = true;
	}

	void Camera3D::setTargetY(float y)
	{
		target.y = y;
		should_update = true;
	}

	void Camera3D::setTargetZ(float z)
	{
		target.z = z;
		should_update = true;
	}

	void Camera3D::setUp(glm::vec3 up)
	{
		this->up = up;
		should_update = true;
	}

	void Camera3D::setUp(float x, float y, float z)
	{
		up.x = x;
		up.y = y;
		up.z = z;
		should_update = true;
	}

	void Camera3D::setUpX(float x)
	{
		up.x = x;
		should_update = true;
	}

	void Camera3D::setUpY(float y)
	{
		up.y = y;
		should_update = true;
	}

	void Camera3D::setUpZ(float z)
	{
		up.z = z;
		should_update = true;
	}

	glm::vec3 Camera3D::getPosition() const
	{
		return position;
	}

	float Camera3D::getPositionX() const
	{
		return position.x;
	}

	float Camera3D::getPositionY() const
	{
		return position.y;
	}

	float Camera3D::getPositionZ() const
	{
		return position.z;
	}

	glm::vec3 Camera3D::getTarget() const
	{
		return target;
	}

	float Camera3D::getTargetX() const
	{
		return target.x;
	}

	float Camera3D::getTargetY() const
	{
		return target.y;
	}

	float Camera3D::getTargetZ() const
	{
		return target.z;
	}

	glm::vec3 Camera3D::getUp() const
	{
		return up;
	}

	float Camera3D::getUpX() const
	{
		return up.x;
	}

	float Camera3D::getUpY() const
	{
		return up.y;
	}

	float Camera3D::getUpZ() const
	{
		return up.z;
	}
}