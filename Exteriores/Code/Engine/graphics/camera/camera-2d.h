#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <Engine/transformable/transformable.h>

namespace Fabr::Graphics
{
	class Camera2D : public Fabr::Math::Transformable
	{
	public:
		Camera2D();
		Camera2D(const Camera2D& other);
		~Camera2D();

	public:
		void use();
	};
}