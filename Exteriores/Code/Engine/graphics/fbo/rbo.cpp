#include <Engine/graphics/fbo/rbo.h>

#include <Windows.h>
#include <GL/glew.h>

namespace Fabr::Graphics
{
	Renderbuffer::Renderbuffer()
	{
		id = 0;
		type = RenderbufferSurfaceType::NONE;
	}

	Renderbuffer::Renderbuffer(const Renderbuffer& other)
	{
		if (id != 0)
		{
			glDeleteRenderbuffers(1, &id);
		}
		id = other.id;
		type = other.type;
	}

	Renderbuffer::~Renderbuffer()
	{
		if (id != 0)
		{
			glDeleteRenderbuffers(1, &id);
			id = 0;
		}
	}

	void Renderbuffer::generateSurface(RenderbufferSurfaceType type, unsigned int width, unsigned int height)
	{
		if (type == RenderbufferSurfaceType::NONE) return;

		if (id != 0)
		{
			FWAR("Renderbuffer exists.");
			return;
		}

		this->type = type;

		glGenRenderbuffers(1, &id);
		glBindRenderbuffer(GL_RENDERBUFFER, id);

		switch (type)
		{
		case RenderbufferSurfaceType::RGBA:
		{
			glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, width, height);
			break;
		}

		case RenderbufferSurfaceType::DEPTH_AND_STENCIL:
		{
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
			break;
		}
		}

		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	unsigned int Renderbuffer::getID() const
	{
		return id;
	}

	RenderbufferSurfaceType Renderbuffer::getSurfaceType() const
	{
		return type;
	}
}