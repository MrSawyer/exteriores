#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>

namespace Fabr::Graphics
{
	enum class RenderbufferSurfaceType
	{
		NONE = 0,
		RGBA, DEPTH_AND_STENCIL
	};

	class Renderbuffer : public virtual Fabr::FaberObject
	{
	private:
		unsigned int id;
		RenderbufferSurfaceType type;

	public:
		Renderbuffer();
		Renderbuffer(const Renderbuffer& other);
		~Renderbuffer();

	public:
		void generateSurface(RenderbufferSurfaceType type, unsigned int width, unsigned int height);
		//void generateMultisampledSurface(RenderbufferSurfaceType type, unsigned int samples, unsigned int width, unsigned int height);

	public:
		unsigned int getID() const;
		RenderbufferSurfaceType getSurfaceType() const;
	};
}