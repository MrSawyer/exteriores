#include <Engine/graphics/fbo/fbo.h>

namespace Fabr::Graphics
{
	Framebuffer::Framebuffer()
	{
		id = 0;
		depth_stencil_attachment = nullptr;
	}

	Framebuffer::Framebuffer(const Framebuffer& other)
	{
		if (id != 0)
		{
			glDeleteFramebuffers(1, &id);
		}
		id = other.id;
		color_attachments = other.color_attachments;
		color_attachments_declaration = other.color_attachments_declaration;
		depth_stencil_attachment = other.depth_stencil_attachment;
	}

	Framebuffer::~Framebuffer()
	{
		if (id != 0)
		{
			glDeleteFramebuffers(1, &id);
			id = 0;
		}
	}

	void Framebuffer::generateFramebuffer()
	{
		glGenFramebuffers(1, &id);
	}

	void Framebuffer::addTextureColorAttachment(Texture* texture)
	{
		if (texture != nullptr && id != 0)
		{
			if (texture->getType() == TextureType::TEXTURE_SURFACE)
			{
				if (texture->getSurfaceType() != TextureSurfaceType::TEXTURE_SURFACE_NONE)
				{
					bool renderbuffer = false;
					if (renderbuffer_attachment != nullptr)
					{
						if (renderbuffer_attachment->getSurfaceType() == RenderbufferSurfaceType::RGBA)
						{
							renderbuffer = true;
						}
					}

					if (!renderbuffer)
					{
						color_attachments.push_back(texture);
						color_attachments_declaration.push_back(GL_COLOR_ATTACHMENT0 + (int)color_attachments.size() - 1);

						glBindFramebuffer(GL_FRAMEBUFFER, id);
						glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + (int)color_attachments.size() - 1, GL_TEXTURE_2D, texture->getID(), 0);
						glDrawBuffers((int)color_attachments_declaration.size(), &color_attachments_declaration[0]);
						glBindFramebuffer(GL_FRAMEBUFFER, 0);
						MessageBox(0, "", "", MB_OK);
					}
				}
			}
		}
	}

	void Framebuffer::setTextureDepthStencilAttachment(Texture* texture)
	{
		if (texture != nullptr && id != 0)
		{
			if (texture->getType() == TextureType::TEXTURE_SURFACE)
			{
				if (texture->getSurfaceType() == TextureSurfaceType::TEXTURE_SURFACE || texture->getSurfaceType() == TextureSurfaceType::TEXTURE_SURFACE_HDR)
				{
					bool renderbuffer = false;
					if (renderbuffer_attachment != nullptr)
					{
						if (renderbuffer_attachment->getSurfaceType() == RenderbufferSurfaceType::DEPTH_AND_STENCIL)
						{
							renderbuffer = true;
						}
					}

					if (!renderbuffer)
					{
						depth_stencil_attachment = texture;

						glBindFramebuffer(GL_FRAMEBUFFER, id);
						glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, texture->getID(), 0);
						glBindFramebuffer(GL_FRAMEBUFFER, 0);
					}
				}
			}
		}
	}

	void Framebuffer::setRenderbufferAttachment(Renderbuffer* renderbuffer)
	{
		if (renderbuffer != nullptr && id != 0)
		{
			switch (renderbuffer->getSurfaceType())
			{
			case RenderbufferSurfaceType::RGBA:
			{
				if (color_attachments.empty())
				{
					renderbuffer_attachment = renderbuffer;

					glBindFramebuffer(GL_FRAMEBUFFER, id);
					glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbuffer->getID());
					glBindFramebuffer(GL_FRAMEBUFFER, 0);
				}

				break;
			}

			case RenderbufferSurfaceType::DEPTH_AND_STENCIL:
			{
				if (depth_stencil_attachment == nullptr)
				{
					renderbuffer_attachment = renderbuffer;

					glBindFramebuffer(GL_FRAMEBUFFER, id);
					glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer->getID());
					glBindFramebuffer(GL_FRAMEBUFFER, 0);
				}

				break;
			}
			}
		}
	}

	void Framebuffer::clearAttachments()
	{
		for (size_t i = 0; i < color_attachments.size(); ++i)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + (int)i, GL_TEXTURE_2D, 0, 0);
		}
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0, 0);

		color_attachments.clear();
		color_attachments_declaration.clear();

		depth_stencil_attachment = nullptr;
		renderbuffer_attachment = nullptr;
	}

	void Framebuffer::setViewport(int x, int y, int width, int height)
	{
		glViewport(0, 0, width, height);
	}

	void Framebuffer::setAsRenderTarget(TargetType type)
	{
		if (id != 0)
		{
			switch (type)
			{
			case TargetType::WRITE_FRAMEBUFFER:
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id);
				break;

			case TargetType::READ_FRAMEBUFFER:
				glBindFramebuffer(GL_READ_FRAMEBUFFER, id);
				break;

			case TargetType::FULL_ACCESS:
				glBindFramebuffer(GL_FRAMEBUFFER, id);
				break;
			}
		}
	}

	void Framebuffer::clearBuffers(SurfaceMask masks)
	{
		glClear(static_cast<GLbitfield>(masks));
	}

	void Framebuffer::setBackgroundColor(glm::vec4 color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void Framebuffer::setDepthClearValue(double depth)
	{
		glClearDepth(depth);
	}

	void Framebuffer::setStencilClearValue(int stencil)
	{
		glClearStencil(stencil);
	}

	void Framebuffer::enableDepthTest(bool enable)
	{
		if (enable) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
	}

	void Framebuffer::enableStencilTest(bool enable)
	{
		if (enable) glEnable(GL_STENCIL_TEST);
		else glDisable(GL_STENCIL_TEST);
	}
}