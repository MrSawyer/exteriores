#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <Engine/system/opengl/surface-bitfields.h>
#include <Engine/graphics/texture/texture.h>
#include <Engine/graphics/fbo/rbo.h>

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>

namespace Fabr::Graphics
{
	using namespace Fabr::System;

	enum class TargetType
	{
		WRITE_FRAMEBUFFER, READ_FRAMEBUFFER, FULL_ACCESS
	};

	class Framebuffer : public virtual Fabr::FaberObject
	{
	private:
		unsigned int id;

	private:
		std::vector <Texture*>	color_attachments;
		std::vector <GLenum>	color_attachments_declaration;

	private:
		Texture* depth_stencil_attachment;
		Renderbuffer* renderbuffer_attachment;

	public:
		Framebuffer();
		Framebuffer(const Framebuffer& other);
		~Framebuffer();

	public:
		void generateFramebuffer();

	public:
		void addTextureColorAttachment(Texture* texture);
		void setTextureDepthStencilAttachment(Texture* texture);
		void setRenderbufferAttachment(Renderbuffer* renderbuffer);
		void clearAttachments();

	public:
		void setViewport(int x, int y, int width, int height);
		void setAsRenderTarget(TargetType type = TargetType::FULL_ACCESS);
		void clearBuffers(SurfaceMask masks);

	public:
		void setBackgroundColor(glm::vec4 color);
		void setDepthClearValue(double depth);
		void setStencilClearValue(int stencil);

	public:
		void enableDepthTest(bool enable);
		void enableStencilTest(bool enable);
	};
}