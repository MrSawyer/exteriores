#pragma once

#include <Engine/flog/flog.h>

namespace Fabr::Graphics
{
	class CubePreset
	{
	private:
		static unsigned int vao, vbo, ebo;

	protected:
		CubePreset() = delete;
		CubePreset(const CubePreset& other) = delete;
		~CubePreset() = delete;

	public:
		static bool generatePreset();
		static void terminatePreset();

	public:
		static void draw();
	};
}