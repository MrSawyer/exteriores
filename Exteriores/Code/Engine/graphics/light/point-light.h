#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <Engine/colorable/colorable.h>

namespace Fabr::Graphics
{
	class PointLight : public Fabr::Math::Colorable
	{
	private:
		glm::vec3 position;

	private:
		float power;
		float attenuation_constant;
		float attenuation_linear;
		float attenuation_quadratic;

	public:
		PointLight();
		PointLight(const PointLight& other);
		~PointLight();

	public:
		void setPower(float power);
		void setAttenuationConstant(float constant);
		void setAttenuationLinear(float linear);
		void setAttenuationQuadratic(float quadratic);

	public:
		float getPower() const;
		float getAttenuationConstant() const;
		float getAttenuationLinear() const;
		float getAttenuationQuadratic() const;

	public:
		void setPosition(glm::vec3 position);
		void setPosition(float x, float y, float z);
		void setPositionX(float x);
		void setPositionY(float y);
		void setPositionZ(float z);

	public:
		glm::vec3 getPosition() const;
		float getPositionX() const;
		float getPositionY() const;
		float getPositionZ() const;
	};
}