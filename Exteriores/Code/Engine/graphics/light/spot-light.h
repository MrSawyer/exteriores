#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <Engine/colorable/colorable.h>

namespace Fabr::Graphics
{
	class SpotLight : public Fabr::Math::Colorable
	{
	private:
		glm::vec3 position;
		glm::vec3 direction;

	private:
		float power;
		float attenuation_constant;
		float attenuation_linear;
		float attenuation_quadratic;

	private:
		float phi;
		float theta;
		float gamma;

	public:
		SpotLight();
		SpotLight(const SpotLight& other);
		~SpotLight();

	public:
		void setPower(float power);
		void setAttenuationConstant(float constant);
		void setAttenuationLinear(float linear);
		void setAttenuationQuadratic(float quadratic);
		void setPhi(float phi);
		void setTheta(float theta);
		void setGamma(float gamma);

	public:
		float getPower() const;
		float getAttenuationConstant() const;
		float getAttenuationLinear() const;
		float getAttenuationQuadratic() const;
		float getPhi() const;
		float getTheta() const;
		float getGamma() const;

	public:
		void setPosition(glm::vec3 position);
		void setPosition(float x, float y, float z);
		void setPositionX(float x);
		void setPositionY(float y);
		void setPositionZ(float z);

	public:
		glm::vec3 getPosition() const;
		float getPositionX() const;
		float getPositionY() const;
		float getPositionZ() const;

	public:
		void setDirection(glm::vec3 direction);
		void setDirection(float x, float y, float z);
		void setDirectionX(float x);
		void setDirectionY(float y);
		void setDirectionZ(float z);

	public:
		glm::vec3 getDirection() const;
		float getDirectionX() const;
		float getDirectionY() const;
		float getDirectionZ() const;
	};
}