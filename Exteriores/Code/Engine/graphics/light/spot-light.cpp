#include <Engine/graphics/light/spot-light.h>

namespace Fabr::Graphics
{
	SpotLight::SpotLight()
	{
		position = glm::vec3(0.0f, 0.0f, 0.0f);
		direction = glm::vec3(0.0f, 0.0f, -1.0f);
		power = 1.0f;
		attenuation_constant = 1.0f;
		attenuation_linear = 0.09f;
		attenuation_quadratic = 0.032f;
		phi = 30.0f;
		theta = 25.0f;
		gamma = 35.0f;
	}

	SpotLight::SpotLight(const SpotLight& other)
	{
		position = other.position;
		direction = other.direction;
		power = other.power;
		attenuation_constant = other.attenuation_constant;
		attenuation_linear = other.attenuation_linear;
		attenuation_quadratic = other.attenuation_quadratic;
		phi = other.phi;
		theta = other.theta;
		gamma = other.gamma;
	}

	SpotLight::~SpotLight() {}

	void SpotLight::setPower(float power)
	{
		this->power = power;
	}

	void SpotLight::setAttenuationConstant(float constant)
	{
		attenuation_constant = constant;
	}

	void SpotLight::setAttenuationLinear(float linear)
	{
		attenuation_linear = linear;
	}

	void SpotLight::setAttenuationQuadratic(float quadratic)
	{
		attenuation_quadratic = quadratic;
	}

	void SpotLight::setPhi(float phi)
	{
		this->phi = phi;
	}

	void SpotLight::setTheta(float theta)
	{
		this->theta = theta;
	}

	void SpotLight::setGamma(float gamma)
	{
		this->gamma = gamma;
	}

	float SpotLight::getPower() const
	{
		return power;
	}

	float SpotLight::getAttenuationConstant() const
	{
		return attenuation_constant;
	}

	float SpotLight::getAttenuationLinear() const
	{
		return attenuation_linear;
	}

	float SpotLight::getAttenuationQuadratic() const
	{
		return attenuation_quadratic;
	}

	float SpotLight::getPhi() const
	{
		return phi;
	}

	float SpotLight::getTheta() const
	{
		return theta;
	}

	float SpotLight::getGamma() const
	{
		return gamma;
	}

	void SpotLight::setPosition(glm::vec3 position)
	{
		this->position = position;
	}

	void SpotLight::setPosition(float x, float y, float z)
	{
		position.x = x;
		position.y = y;
		position.z = z;
	}

	void SpotLight::setPositionX(float x)
	{
		position.x = x;
	}

	void SpotLight::setPositionY(float y)
	{
		position.y = y;
	}

	void SpotLight::setPositionZ(float z)
	{
		position.z = z;
	}

	glm::vec3 SpotLight::getPosition() const
	{
		return position;
	}

	float SpotLight::getPositionX() const
	{
		return position.x;
	}

	float SpotLight::getPositionY() const
	{
		return position.y;
	}

	float SpotLight::getPositionZ() const
	{
		return position.z;
	}

	void SpotLight::setDirection(glm::vec3 direction)
	{
		this->direction = direction;
	}

	void SpotLight::setDirection(float x, float y, float z)
	{
		direction.x = x;
		direction.y = y;
		direction.z = z;
	}

	void SpotLight::setDirectionX(float x)
	{
		direction.x = x;
	}

	void SpotLight::setDirectionY(float y)
	{
		direction.y = y;
	}

	void SpotLight::setDirectionZ(float z)
	{
		direction.z = z;
	}

	glm::vec3 SpotLight::getDirection() const
	{
		return direction;
	}

	float SpotLight::getDirectionX() const
	{
		return direction.x;
	}

	float SpotLight::getDirectionY() const
	{
		return direction.y;
	}

	float SpotLight::getDirectionZ() const
	{
		return direction.z;
	}
}