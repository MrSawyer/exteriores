#include <Engine/graphics/light/point-light.h>

namespace Fabr::Graphics
{
	PointLight::PointLight()
	{
		position = glm::vec3(0.0f, 0.0f, 0.0f);
		power = 1.0f;
		attenuation_constant = 1.0f;
		attenuation_linear = 0.09f;
		attenuation_quadratic = 0.032f;
	}

	PointLight::PointLight(const PointLight& other)
	{
		position = other.position;
		power = other.power;
		attenuation_constant = other.attenuation_constant;
		attenuation_linear = other.attenuation_linear;
		attenuation_quadratic = other.attenuation_quadratic;
	}

	PointLight::~PointLight() {}

	void PointLight::setPower(float power)
	{
		this->power = power;
	}

	void PointLight::setAttenuationConstant(float constant)
	{
		attenuation_constant = constant;
	}

	void PointLight::setAttenuationLinear(float linear)
	{
		attenuation_linear = linear;
	}

	void PointLight::setAttenuationQuadratic(float quadratic)
	{
		attenuation_quadratic = quadratic;
	}

	float PointLight::getPower() const
	{
		return power;
	}

	float PointLight::getAttenuationConstant() const
	{
		return attenuation_constant;
	}

	float PointLight::getAttenuationLinear() const
	{
		return attenuation_linear;
	}

	float PointLight::getAttenuationQuadratic() const
	{
		return attenuation_quadratic;
	}

	void PointLight::setPosition(glm::vec3 position)
	{
		this->position = position;
	}

	void PointLight::setPosition(float x, float y, float z)
	{
		position.x = x;
		position.y = y;
		position.z = z;
	}

	void PointLight::setPositionX(float x)
	{
		position.x = x;
	}

	void PointLight::setPositionY(float y)
	{
		position.y = y;
	}

	void PointLight::setPositionZ(float z)
	{
		position.z = z;
	}

	glm::vec3 PointLight::getPosition() const
	{
		return position;
	}

	float PointLight::getPositionX() const
	{
		return position.x;
	}

	float PointLight::getPositionY() const
	{
		return position.y;
	}

	float PointLight::getPositionZ() const
	{
		return position.z;
	}
}