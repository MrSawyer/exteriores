#pragma once

#include <Engine/flog/flog.h>
#include <Engine/faber-object/faber-object.h>
#include <GLM/glm.hpp>

namespace Fabr::Math
{
	class Colorable : public virtual Fabr::FaberObject
	{
	private:
		glm::vec4 color;

	public:
		Colorable();
		Colorable(const Colorable& other);
		virtual ~Colorable() = default;

	public:
		void setColor(glm::vec4 color);
		void setColor(float r, float g, float b, float a);
		void setColor(glm::vec3 color);
		void setColor(float r, float g, float b);
		void setColorR(float r);
		void setColorG(float g);
		void setColorB(float b);
		void setColorA(float a);

	public:
		glm::vec4	getColorRGBA() const;
		glm::vec3	getColorRGB() const;
		float		getColorR() const;
		float		getColorG() const;
		float		getColorB() const;
		float		getColorA() const;
	};
}