#include <Engine/colorable/colorable.h>

namespace Fabr::Math
{
	Colorable::Colorable()
	{
		color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	Colorable::Colorable(const Colorable& other)
	{
		color = other.color;
	}

	void Colorable::setColor(glm::vec4 color)
	{
		this->color = color;
	}

	void Colorable::setColor(float r, float g, float b, float a)
	{
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = a;
	}

	void Colorable::setColor(glm::vec3 color)
	{
		this->color.r = color.r;
		this->color.g = color.g;
		this->color.b = color.b;
	}

	void Colorable::setColor(float r, float g, float b)
	{
		color.r = r;
		color.g = g;
		color.b = b;
	}

	void Colorable::setColorR(float r)
	{
		color.r = r;
	}

	void Colorable::setColorG(float g)
	{
		color.g = g;
	}

	void Colorable::setColorB(float b)
	{
		color.b = b;
	}

	void Colorable::setColorA(float a)
	{
		color.a = a;
	}

	glm::vec4 Colorable::getColorRGBA() const
	{
		return color;
	}

	glm::vec3 Colorable::getColorRGB() const
	{
		return glm::vec3(color.r, color.g, color.b);
	}

	float Colorable::getColorR() const
	{
		return color.r;
	}

	float Colorable::getColorG() const
	{
		return color.g;
	}

	float Colorable::getColorB() const
	{
		return color.b;
	}

	float Colorable::getColorA() const
	{
		return color.a;
	}
}