#include <Engine/console/console.h>
#include <Engine/flog/flog.h>

#include <iostream>
#include <Windows.h>

namespace Fabr
{
#if F_LOG_ENABLE
	void createConsole()
	{
		AllocConsole();
		AttachConsole(GetCurrentProcessId());
		freopen_s((FILE**)stdout, "CON", "w", stdout);
		freopen_s((FILE**)stderr, "CON", "w", stderr);
		freopen_s((FILE**)stdin, "CON", "r", stdin);
		std::ios::sync_with_stdio();
		std::cout.clear();
		std::clog.clear();
		std::cerr.clear();
		std::cin.clear();

		const HANDLE	console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
		const HWND		console_window = GetConsoleWindow();

		RECT primary_client;
		GetClientRect(GetDesktopWindow(), &primary_client);

		const int screen_width = (int)(primary_client.right - primary_client.left);
		const int screen_height = (int)(primary_client.bottom - primary_client.top);

		SetConsoleTitle("Exteriores - Console Window");
		SetWindowLong(console_window, GWL_STYLE, WS_CAPTION | WS_BORDER);

		RECT console_rect;
		GetWindowRect(console_window, &console_rect);

		const int center_pos_x = screen_width / 2 - (int)(console_rect.right - console_rect.left) / 2;
		const int center_pos_y = screen_height / 2 - (int)(console_rect.bottom - console_rect.top) / 2;

		SetWindowPos(console_window, 0, center_pos_x, center_pos_y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

		ShowWindow(console_window, SW_SHOW);
		UpdateWindow(console_window);
	}

	void deleteConsole()
	{
		FreeConsole();
	}
#else
	void createConsole() {}

	void deleteConsole() {}
#endif
}