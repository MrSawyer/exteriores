#pragma once
#include <Engine/gui/gui-object.h>
#include <Engine/graphics/text/text.h>

namespace Fabr::Gui
{
	class GUIText : public virtual GUIObject , public virtual Fabr::Graphics::Text
	{
	public:
		void update()override;
		void display()override;

	private:
	};

}// namespace Fabr::Gui

