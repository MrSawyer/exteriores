#include "gui-object.h"

namespace Fabr::Gui {
	GUIObject::GUIObject()
	{
		visible_ = true;
	}

	void GUIObject::addChild(GUIObject* obj)
	{
		children_.push_back(obj);
	}

	bool GUIObject::isPointInside(glm::vec2 point)
	{
		glm::vec3 LT = getPosition() - (getScale()/2.0f);
		glm::vec3 BD = getPosition() + (getScale()/2.0f);
		
		if (point.x >= LT.x && point.y >= LT.y && point.x <= BD.x && point.y <= BD.y)return true;

		return false;
	}

	void GUIObject::update()
	{
		if (!visible_)return;

		for (auto& obj : children_)
		{
			obj->update();
		}
	}

	void GUIObject::display()
	{
		if (!visible_)return;

		gui_shader_.use();
		//Sprite::draw();

		for (auto& obj : children_)
		{
			obj->display();
		}
	}

	void GUIObject::show()
	{
		visible_ = true;
	}

	void GUIObject::hide()
	{
		visible_ = false;
	}
	void GUIObject::initializeGUIShader(const char* vs_path, const char* fs_path)
	{
		gui_shader_.loadFromFiles(vs_path, fs_path);
	}

	void GUIObject::terminateGUIShader()
	{
		gui_shader_.terminateShader();
	}

	bool GUIObject::isVisible()
	{
		return visible_;
	}
}
