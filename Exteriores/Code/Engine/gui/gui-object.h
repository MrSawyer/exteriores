#pragma once
#include <Engine/faber-object/faber-object.h>
#include <Engine/transformable/transformable.h>
#include <Engine/graphics/sprite/sprite.h>
#include <Engine/graphics/shader/shader.h>

#include <memory>
#include <vector>

namespace Fabr::Gui
{
	class GUIObject : virtual public Fabr::FaberObject, virtual public Fabr::Graphics::Sprite
	{
	public:
		GUIObject();

		void addChild(GUIObject * obj);

		bool isPointInside(glm::vec2 point);

		virtual void update();
		virtual void display();

		void show();
		void hide();

		static void initializeGUIShader(const char * vs_path, const char * fs_path);
		static void terminateGUIShader(); // TERMIFIX

		bool isVisible();

	protected:
		inline static Fabr::Graphics::Shader gui_shader_;
		bool visible_;

	private:
		std::vector<GUIObject*> children_;
	};

}// 