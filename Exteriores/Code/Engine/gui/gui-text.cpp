#include "gui-text.h"
#include <Game/assets/assets.h>


void Fabr::Gui::GUIText::update()
{
}

void Fabr::Gui::GUIText::display()
{
	if (!visible_)return;
	Extr::Assets::getAssets()->getShaders().find("TEXT")->second.get()->use();
	Fabr::Graphics::Text::draw();
}
