#include "button.h"
#include <Engine/flog/flog.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>

namespace Fabr::Gui {

	void Button::update()
	{
		if (!visible_)return;

		System::Input* inputhandler = System::PTVGlobalContainer::getActiveInput();
		if (!inputhandler)
		{
			FERR("No active input");
			return;
		}

		if (inputhandler->keyReleased(System::MouseButtons::LEFT_BUTTON) && isPointInside(inputhandler->getCursorPosition()))
		{
			if (onClick)onClick();
			else FERR("There is no action asssigned to this button");
		}
	}

	void Button::display()
	{
		if (!visible_)return;

		gui_shader_.use();
		draw();
	}

	void Button::setOnClick(std::function<void()> event)
	{
		if (!event)
		{
			FERR("Assagning empty event to button");
			return;
		}
		onClick = event;
	}
}