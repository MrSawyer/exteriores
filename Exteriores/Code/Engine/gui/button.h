#pragma once
#include <Engine/gui/gui-object.h>
#include <functional>

namespace Fabr::Gui
{
	class Button : public GUIObject
	{
	public:
		void update()override;
		void display()override;

		void setOnClick(std::function<void()> event);
	private:
		std::function<void()> onClick;
	};

}// namespace Fabr::Gui

