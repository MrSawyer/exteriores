#include <Engine/engine.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/graphics/texture/texture.h>
#include <Engine/graphics/sprite/sprite.h>
#include <Engine/graphics/camera/camera.h>
#include <Engine/graphics/light/light.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Engine/graphics/cube-preset/cube-preset.h>
#include <Engine/graphics/text/character-preset.h>
#include <Engine/graphics/model/model.h>
#include <Engine/graphics/text/text.h>

#include <Game/game.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <iostream>
#include <memory>

using namespace std;
using namespace Fabr::System;
using namespace Fabr::Graphics;

class PipelineTest : public Pipeline
{
private:
	unique_ptr<Camera3D>	camera;

	unique_ptr<Shader>		backgroundshader, cubeshader, textshader;

	unique_ptr<Texture>		backgroundtex0, backgroundtex1, backgroundtex2;
	unique_ptr<Sprite>		background0, background1, background2;

	unique_ptr<Model>		target;
	unique_ptr<Model>		target2;
	unique_ptr<PointLight>	light;

	unique_ptr<Font>		calibri;
	unique_ptr<Text>		example;

public:
	bool onInitialize(int eventcode) override
	{
		if (eventcode == 0)
		{
			backgroundshader = make_unique<Shader>();
			backgroundshader->loadFromFiles("Assets/Shaders/background-onepass.vert", "Assets/Shaders/background-onepass.frag");

			cubeshader = make_unique<Shader>();
			cubeshader->loadFromFiles("Assets/Shaders/cube-preset.vert", "Assets/Shaders/cube-preset.frag");

			textshader = make_unique<Shader>();
			textshader->loadFromFiles("Assets/Shaders/text-shader.vert", "Assets/Shaders/text-shader.frag");

			backgroundtex0 = make_unique<Texture>();
			backgroundtex0->loadFromFile("Assets/Textures/bg0.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			backgroundtex1 = make_unique<Texture>();
			backgroundtex1->loadFromFile("Assets/Textures/bg1.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			backgroundtex2 = make_unique<Texture>();
			backgroundtex2->loadFromFile("Assets/Textures/bg2.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			background0 = make_unique<Sprite>();
			background0->setTexture(backgroundtex0.get());
			background0->setScale(1920.0f, 1080.0f, 1.0f);
			background0->setScale(background0->getScale() * 2.0f);

			background1 = make_unique<Sprite>();
			background1->setTexture(backgroundtex1.get());
			background1->setScale(1920.0f, 1080.0f, 1.0f);
			background1->setScale(background1->getScale() * 2.0f);

			background2 = make_unique<Sprite>();
			background2->setTexture(backgroundtex2.get());
			background2->setScale(1920.0f, 1080.0f, 1.0f);
			background2->setScale(background2->getScale() * 2.0f);

			camera = make_unique<Camera3D>();
			camera->setPosition(0.0f, 3.0f, 6.0f);
			camera->setTarget(0.0f, 0.0f, 0.0f);
			camera->use();

			target = make_unique<Model>();
			target->loadFromFile("Assets/Models/sun0.obj");
			target->setRotationY(-45.0f);

			target2 = make_unique<Model>();
			target2->loadFromFile("Assets/Models/target.obj");
			target2->setRotationY(-45.0f);
			target2->setScale(0.3f, 0.3f, 0.3f);
			target2->setPositionX(-4.0f);

			light = make_unique<PointLight>();
			//light->setPosition(5.0f, 5.0f, 5.0f);
			light->setPower(3.0f);

			calibri = make_unique<Font>();
			calibri->loadFromFile("Assets/Fonts/calibri.ttf", 36);

			example = make_unique <Text>();
			example->setFont(calibri.get());
			example->setText("Exteriores:\nSuper Crap FONT");
			example->setColor(1.0f, 0.3f, 1.0f);

			RectanglePreset::generatePreset();
			CharacterPreset::generatePreset();
			std::cout << "DEBUG INFO: Utworzono dane pipelinu" << std::endl;
		}

		return true;
	}

	bool onTerminate(int eventcode) override
	{
		if (eventcode == 0)
		{
			example.reset();
			calibri.reset();
			light.reset();
			target.reset();
			camera.reset();
			background2.reset();
			background1.reset();
			background0.reset();
			backgroundtex2.reset();
			backgroundtex1.reset();
			backgroundtex0.reset();
			textshader.reset();
			cubeshader.reset();
			backgroundshader.reset();

			CharacterPreset::terminatePreset();
			RectanglePreset::terminatePreset();
			std::cout << "DEBUG INFO: Zwolniono dane pipelinu" << std::endl;
		}

		return true;
	}

	void onResize(float width, float height) override
	{
		background0->setScale(2.0f * width, 2.0f * height, 1.0f);
		background1->setScale(2.0f * width, 2.0f * height, 1.0f);
		background2->setScale(2.0f * width, 2.0f * height, 1.0f);

		example->setPosition(width / 2.0f - 300.0f, height / 2.0f - 50.0f, 0.0f);
	}

public:
	void onLogicTick() override
	{
		Input* inputhandler = PTVGlobalContainer::getActiveInput();

		glm::vec2 mp = inputhandler->getCursorPosition();
		mp.x = mp.x / PTVGlobalContainer::getSurfaceWidth() * 2.0f;
		mp.y = mp.y / PTVGlobalContainer::getSurfaceHeight() * 2.0f;

		background1->setPosition(2.5f * mp.x, 2.5f * mp.y, 0.0f);
		background2->setPosition(10.0f * mp.x, 10.0f * mp.y, 0.0f);

		if (inputhandler->keyPressed('A'))
		{
			target->setRotationY(target->getRotationY() - 90.0f * (1.0f / PTVGlobalContainer::getMaxFPS()));
		}
		else if (inputhandler->keyPressed('D'))
		{
			target->setRotationY(target->getRotationY() + 90.0f * (1.0f / PTVGlobalContainer::getMaxFPS()));
		}

		if (inputhandler->keyPressed(MouseButtons::RIGHT_BUTTON))
		{
			target2->setPosition(camera->getPosition() + 5.0f * inputhandler->getCursorDirection());
		}
	}

	void onRendererTick() override
	{
		PTVGlobalContainer::getActiveSurface()->clearBuffers(SurfaceMask::COLOR_BUFFER | SurfaceMask::DEPTH_BUFFER);


		glDisable(GL_DEPTH_TEST);
		backgroundshader->use();
		background0->draw();
		background1->draw();
		background2->draw();

		glEnable(GL_DEPTH_TEST);
		cubeshader->use();
		cubeshader->send(camera->getPosition(), "u_cameraposition");
		cubeshader->send(light.get(), 0);
		cubeshader->send(1, "u_pointlights_count");

		cubeshader->send(1, "flipnormals");
		target->draw();

		cubeshader->send(0, "flipnormals");
		target2->draw();

		glDisable(GL_DEPTH_TEST);
		textshader->use();
		example->draw();

		PTVGlobalContainer::getActiveSurface()->swapBuffers();
	}
};

int __stdcall WinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE previnstance, _In_ LPSTR cmdline, _In_ int cmdshow)
{
	Fabr::createConsole();

	Instance::setHandle(instance);

	unique_ptr<PipelineTest> pipeline = make_unique<PipelineTest>();
	unique_ptr<Window> window = make_unique<Window>();

	window->setPipeline(pipeline.get());

	int result = window->launchLoop();

	window.reset();

	system("pause");
	Fabr::deleteConsole();

	return result;
}