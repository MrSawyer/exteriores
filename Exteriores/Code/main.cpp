#include <Engine/engine.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/graphics/texture/texture.h>
#include <Engine/graphics/sprite/sprite.h>
#include <Engine/graphics/camera/camera.h>
#include <Engine/graphics/light/light.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Engine/graphics/cube-preset/cube-preset.h>
#include <Engine/graphics/text/character-preset.h>
#include <Engine/graphics/model/model.h>
#include <Engine/graphics/text/text.h>
#include <Engine/graphics/path-preset/path-preset.h>
#include <Engine/graphics/fbo/fbo.h>

#include <Game/game.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <iostream>
#include <memory>

using namespace std;
using namespace Fabr::System;
using namespace Fabr::Graphics;

class Exteriores : public Pipeline
{
private:
	unique_ptr<Camera3D>	camera;

	unique_ptr<Shader>		backgroundshader;

	unique_ptr<Texture>		backgroundtex0, backgroundtex1, backgroundtex2, backgroundtex3;
	unique_ptr<Sprite>		background0, background1, background2, background3;

public:
	bool onInitialize(int eventcode) override
	{
		if (eventcode == 0)
		{
			backgroundshader = make_unique<Shader>();
			backgroundshader->loadFromFiles("Assets/Shaders/background-onepass.vert", "Assets/Shaders/background-onepass.frag");

			backgroundtex0 = make_unique<Texture>();
			backgroundtex0->loadFromFile("Assets/Textures/bg0.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			backgroundtex1 = make_unique<Texture>();
			backgroundtex1->loadFromFile("Assets/Textures/bg1.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			backgroundtex2 = make_unique<Texture>();
			backgroundtex2->loadFromFile("Assets/Textures/bg2.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			backgroundtex3 = make_unique<Texture>();
			backgroundtex3->loadFromFile("Assets/Textures/bg3.png", TextureImageType::TEXTURE_IMAGE, true, TextureFiltering::LINEAR, TextureFiltering::LINEAR, TextureWrapping::MIRRORED_REPEAT, TextureWrapping::MIRRORED_REPEAT);

			background0 = make_unique<Sprite>();
			background0->setTexture(backgroundtex0.get());
			background0->setScale(1920.0f, 1080.0f, 1.0f);
			background0->setScale(background0->getScale() * 2.0f);

			background1 = make_unique<Sprite>();
			background1->setTexture(backgroundtex1.get());
			background1->setScale(1920.0f, 1080.0f, 1.0f);
			background1->setScale(background1->getScale() * 2.0f);

			background2 = make_unique<Sprite>();
			background2->setTexture(backgroundtex2.get());
			background2->setScale(1920.0f, 1080.0f, 1.0f);
			background2->setScale(background2->getScale() * 2.0f);

			background3 = make_unique<Sprite>();
			background3->setTexture(backgroundtex3.get());
			background3->setScale(1920.0f, 1080.0f, 1.0f);
			background3->setScale(background3->getScale() * 2.0f);

			camera = make_unique<Camera3D>();
			camera->setPosition(0.0f, 5000.0f, -1000.0f);
			camera->setTarget(0.0f, 0.0f, 0.0f);
			camera->use();

			RectanglePreset::generatePreset();
			CharacterPreset::generatePreset();
			PathPreset::generatePreset();

			Extr::GameManager::initGame();
			Fabr::Gui::GUIObject::initializeGUIShader("Assets/Shaders/sprite-onepass.vert", "Assets/Shaders/sprite-onepass.frag");
		}

		return true;
	}

	bool onTerminate(int eventcode) override
	{
		if (eventcode == 0)
		{
			Fabr::Gui::GUIObject::terminateGUIShader();
			Extr::GameManager::terminateGame();

			PathPreset::terminatePreset();
			CharacterPreset::terminatePreset();
			RectanglePreset::terminatePreset();

			camera.reset();

			background3.reset();
			background2.reset();
			background1.reset();
			background0.reset();

			backgroundtex3.reset();
			backgroundtex2.reset();
			backgroundtex1.reset();
			backgroundtex0.reset();

			backgroundshader.reset();
		}

		return true;
	}

	void onResize(float width, float height) override
	{
		background0->setScale(3.0f * width, 3.0f * height, 1.0f);
		background1->setScale(3.0f * width, 3.0f * height, 1.0f);
		background2->setScale(3.0f * width, 3.0f * height, 1.0f);
		background3->setScale(3.0f * width, 3.0f * height, 1.0f);

		Extr::GameManager::handleResize();
	}

public:
	void onLogicTick() override
	{
		glm::vec2 cursorposition = PTVGlobalContainer::getActiveInput()->getCursorPosition();
		glm::vec2 clipped = glm::vec2(cursorposition.x / PTVGlobalContainer::getSurfaceWidth() * 2.0f, cursorposition.y / PTVGlobalContainer::getSurfaceHeight() * 2.0f);

		background1->setPosition(5.0f * clipped.x, 5.0f * clipped.y, 0.0f);
		background2->setPosition(10.0f * clipped.x, 10.0f * clipped.y, 0.0f);
		background3->setPosition(20.0f * clipped.x, 20.0f * clipped.y, 0.0f);

		Extr::GameManager::handleCamera();

		Extr::GameManager::updateLogic();

		Extr::GameManager::updateMenu();
	}

	void onRendererTick() override
	{
		PTVGlobalContainer::getActiveSurface()->clearBuffers(SurfaceMask::COLOR_BUFFER | SurfaceMask::DEPTH_BUFFER);

		PTVGlobalContainer::getActiveSurface()->enableDepthTest(false);
		backgroundshader->use();
		background0->draw();
		background1->draw();
		background2->draw();
		background3->draw();

		PTVGlobalContainer::getActiveSurface()->enableDepthTest(true);
		Extr::GameManager::renderGame();

		PTVGlobalContainer::getActiveSurface()->enableDepthTest(false);
		Extr::GameManager::displayMenu();

		PTVGlobalContainer::getActiveSurface()->swapBuffers();
	}
};

int __stdcall WinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE previnstance, _In_ LPSTR cmdline, _In_ int cmdshow)
{
	srand((unsigned)time(NULL));

	Fabr::createConsole();
	Instance::setHandle(instance);

	unique_ptr<Exteriores> pipeline = make_unique<Exteriores>();
	unique_ptr<Window> window = make_unique<Window>();

	window->setPipeline(pipeline.get());

	int result = window->launchLoop();

	window.reset();
	pipeline.reset();

	::system("pause");
	Fabr::deleteConsole();

	return result;
}