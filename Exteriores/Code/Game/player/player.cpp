#include "player.h"

#include <Game/world/world.h>
#include <Game/assets/assets.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Game/game-state/game-state.h>

namespace Extr {

	std::unique_ptr<Player> player = std::make_unique<Player>();

	const float PLAYER_MASS = 0.05f;

	const float THRUST_TICK = 0.07f;



	const float THRUST_FORCE[3] = { 0.0005f, 0.001f, 0.0012f };

	const float BOMB_FORCE = 0.3f;

	const unsigned int PATH_POINTS = 12;

	void Player::resetPlayer()
	{
		assignModel(Assets::getAssets()->getPlayerModel().get());
		setShader(Assets::getAssets()->getShaders().begin()->second.get());
		force = glm::vec3(0, 0, 0);
		setPosition(0, 0, 0);
		setScale(50, 50, 50);
		setRotationX(180);
		active_thrusters.clear();
		state = PlayerState::MOVEMENT;

		eq.anchor = 3;
		eq.bombs = 2;
		eq.pulse_inverter = 1;
		eq.radars = 3;
		eq.teleport = 1;
		eq.thruster[0] = 100.0f;
		eq.thruster[1] = 100.0f;
		eq.thruster[2] = 100.0f;
	}

	void Extr::Player::update()
	{
		glm::vec3 cumulative_force = force;
		

		/*
		* Grawitacja
		*/
		cumulative_force += World::getLevel()->calculateGravity(getPosition());


		if (World::getLevel()->colisionWithPlanet(getPosition(), getScaleX()))
		{
			state = PlayerState::DESTROYED;
		}

		/*
		* Sprawd� czy dopalacze dzia�aj�
		*/
		int thruster_id = 0; 
		while (thruster_id < active_thrusters.size())
		{
			if (active_thrusters[thruster_id].actual_tick >= active_thrusters[thruster_id].complete_tick)
			{
				active_thrusters.erase(active_thrusters.begin() + thruster_id);
			}
			else thruster_id++;
		}
		
		
		/*
		* Dodaj moc dopalaczy
		*/
		for (unsigned int i = 0; i < active_thrusters.size(); i++)
		{
			state = PlayerState::THRUSTER;
			active_thrusters[i].actual_tick += THRUST_TICK;
			if (eq.thruster[active_thrusters[i].power] >= THRUST_TICK) {
				eq.thruster[active_thrusters[i].power] -= THRUST_TICK; // zabierz paliwo
				cumulative_force += THRUST_FORCE[active_thrusters[i].power] * active_thrusters[i].force_vector;
			}
		}
		
		/*
		* Kolizje z orders
		*/
		const Extr::Order * order = World::getLevel()->collisionWithOrder(getPosition(), getScaleX());

		if (order != nullptr)
		{

			/*
			* Thrustery
			*/
			if (order->type == ItemType::THRUSTER1 || order->type == ItemType::THRUSTER2 || order->type == ItemType::THRUSTER3)
			{
				ActiveThruster new_thrust;

				new_thrust.actual_tick = 0;
				new_thrust.complete_tick = glm::length(order->destination - order->getPosition()) / 30.0f;;
				new_thrust.force_vector = glm::normalize(order->destination - order->getPosition());

				if (order->type == ItemType::THRUSTER1)new_thrust.power = 0;
				if (order->type == ItemType::THRUSTER2)new_thrust.power = 1;
				if (order->type == ItemType::THRUSTER3)new_thrust.power = 2;


				active_thrusters.push_back(new_thrust);
				state = PlayerState::MOVEMENT;
			}

			/*
			* Bomby
			*/
			if (order->type == ItemType::BOMB)
			{
				cumulative_force += BOMB_FORCE * glm::normalize(order->destination - order->getPosition());
				state = PlayerState::MOVEMENT;
			}

			/*
			* Teleporty
			*/
			if (order->type == ItemType::TELEPORT)
			{
				setPosition(order->destination);
				state = PlayerState::MOVEMENT;
			}

			/*
			* Pulse invertery
			*/
			if (order->type == ItemType::PULSEINVERTER)
			{
				cumulative_force *= -1;
			}

			/*
			* Kotwice
			*/
			if (order->type == ItemType::ANCHOR)
			{
				state = PlayerState::STOP;
			}



			World::getLevel()->destroyOrder(order);
		}

		/*
		* Kolizje z POI
		*/
		const World::PointOfInterest* POI = World::getLevel()->collisionWithPointOfInterest(getPosition(), getScaleX());
		if (POI != nullptr)
		{
			ItemType item = POI->getItem();

			if (item == ItemType::THRUSTER1)eq.thruster[0] += rand()% 100 + 100;
			if (item == ItemType::THRUSTER2)eq.thruster[1] += rand() % 100 + 100;;
			if (item == ItemType::THRUSTER3)eq.thruster[2] += rand() % 100 + 100;;
			if (item == ItemType::BOMB)eq.bombs += rand() % 3 + 1;
			if (item == ItemType::TELEPORT)eq.teleport += rand() % 3 + 1;
			if (item == ItemType::PULSEINVERTER)eq.pulse_inverter += rand() % 3 + 1;
			if (item == ItemType::RADAR)eq.radars += rand() % 3 + 1;


			World::getLevel()->destroyPointOfInterest(POI);
		}

		/*
		* Kolizje z celami
		*/
		const World::Survivor* target = World::getLevel()->collisionWithSurvivors(getPosition(), getScaleX());
		if (target != nullptr)
		{
			GameManager::increaseTakenSurvivors();
			World::getLevel()->destroySurvivor(target);
		}
		
		/*
		* Player zatrzymany
		*/
		if (state == PlayerState::STOP)
		{
			force = glm::vec3(0, 0, 0);
			active_thrusters.clear();
			return;
		}

		//rot Y
		glm::vec3 nrm = glm::normalize(cumulative_force);
		if (abs(nrm.x) > glm::epsilon<float>())
		{
			setRotationY(atan2(nrm.z,  nrm.x) * (180.0f / glm::pi<float>()) + 180);
		}

		force = cumulative_force;

		setPosition(getPosition() + (100.0f * force / (PLAYER_MASS * Fabr::System::PTVGlobalContainer::getMaxFPS())));

	}

	glm::vec3 Player::getForce()
	{
		return force;
	}

	EQ & Player::getEQ()
	{
		return eq;
	}

	PlayerState Player::getState()
	{
		return state;
	}


	Player* getPlayer()
	{
		return player.get();
	}

	std::vector<glm::vec3> Player::getPath()const
	{
		if (!player)return std::vector<glm::vec3>();


		std::vector<ActiveThruster> thrusters_tmp = active_thrusters;
		EQ eq_tmp = eq;

		std::vector<glm::vec3> force_path;

		std::vector<glm::vec3> actual_path;

		force_path.push_back(getPlayer()->getPosition());
		
		glm::vec3 cumulated_force = getPlayer()->getForce();

		for (unsigned int i = 1; i < PATH_POINTS * Fabr::System::PTVGlobalContainer::getMaxFPS(); i++)
		{
			force_path.push_back(force_path[i-1] + (100.0f * cumulated_force / (PLAYER_MASS * Fabr::System::PTVGlobalContainer::getMaxFPS())));
			
			cumulated_force += World::getLevel()->calculateGravity(force_path[i]);

			/*
			* thrusters
			*/
			int thruster_id = 0;
			while (thruster_id < thrusters_tmp.size())
			{
				if (thrusters_tmp[thruster_id].actual_tick > thrusters_tmp[thruster_id].complete_tick)
				{
					thrusters_tmp.erase(thrusters_tmp.begin() + thruster_id);
				}
				else thruster_id++;
			}

			for (unsigned int i = 0; i < thrusters_tmp.size(); i++)
			{
				thrusters_tmp[i].actual_tick += THRUST_TICK;
				if (eq_tmp.thruster[thrusters_tmp[i].power] >= THRUST_TICK) {
					eq_tmp.thruster[thrusters_tmp[i].power] -= THRUST_TICK; // zabierz paliwo
					cumulated_force += THRUST_FORCE[thrusters_tmp[i].power] * thrusters_tmp[i].force_vector;
				}
			}


			if (i % 10 == 0)actual_path.push_back(force_path[i]);

		}

		return std::move(actual_path);
	}

}
