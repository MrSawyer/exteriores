#pragma once
#include <Game/game-object/game-actor.h>
#include <Engine/transformable/transformable.h>

#include <vector>


namespace Extr
{
	enum class PlayerState
	{
		MOVEMENT, STOP, THRUSTER, DESTROYED, LOST
	};


	struct EQ
	{
		float thruster[3];
		unsigned int bombs, radars, anchor, teleport, pulse_inverter;
	};

	struct ActiveThruster
	{
		glm::vec3 force_vector;
		float actual_tick, complete_tick;
		unsigned int power;
	};

	class Player final : public virtual Extr::GameActor
	{
	public:
		void resetPlayer();
		void update();

		glm::vec3 getForce();
		
		EQ & getEQ();

		PlayerState getState();
		std::vector<glm::vec3> getPath()const;
	private:
		glm::vec3 force;
		PlayerState state;

		std::vector<ActiveThruster> active_thrusters;

		EQ eq;
	};

	Player* getPlayer();
	std::vector<glm::vec3> & getPlayerPath();
}
