#pragma once

#include <Game/game-object/game-actor.h>
#include <Game/items/items.h>

#include <GLM/glm.hpp>

namespace Extr
{
	struct Order : Extr::GameActor
	{
		glm::vec3 destination;
		ItemType type = ItemType::NONE;
	};
}