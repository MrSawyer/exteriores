#include "assets.h"

namespace Extr::Assets
{

	std::unique_ptr<Assets> assets = std::make_unique<Assets>();

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& Assets::getSunsModels()
	{
		return suns_models;
	}

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& Assets::getPlanetsModels()
	{
		return planets_models;
	}

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& Assets::getPOIModels()
	{
		return POI_models;
	}

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& Assets::getOrdersModels()
	{
		return orders_models;
	}

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& Assets::getSurvivorsModels()
	{
		return survivors_models;
	}

	const std::unique_ptr<Fabr::Graphics::Model>& Assets::getPlayerModel()
	{
		return player_model;
	}

	const std::map<std::string, std::unique_ptr<Fabr::Graphics::Shader>>& Assets::getShaders()
	{
		return shaders;
	}

	void Assets::loadAssets()
	{

		{
			std::unique_ptr<Fabr::Graphics::Shader> loaded_shader = std::make_unique<Fabr::Graphics::Shader>();
			loaded_shader->loadFromFiles("Assets/Shaders/cube-preset.vert", "Assets/Shaders/cube-preset.frag");
			shaders.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Shader>>("3D", std::move(loaded_shader)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Shader> loaded_shader = std::make_unique<Fabr::Graphics::Shader>();
			loaded_shader->loadFromFiles("Assets/Shaders/text-shader.vert", "Assets/Shaders/text-shader.frag");
			shaders.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Shader>>("TEXT", std::move(loaded_shader)));
		}

		{
			player_model = std::make_unique<Fabr::Graphics::Model>();
			player_model->loadFromFile("Assets/Models/player-ship.obj");
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/sun0.obj");
			suns_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("SUN0", std::move(loaded_model)));
		}
		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/sun1.obj");
			suns_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("SUN1", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/planet0.obj");
			planets_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("PLANET0", std::move(loaded_model)));
		}
		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/planet1.obj");
			planets_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("PLANET1", std::move(loaded_model)));
		}


		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/POI0.obj");
			POI_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("POI0", std::move(loaded_model)));
		}
		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/POI1.obj");
			POI_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("POI1", std::move(loaded_model)));
		}


		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/target.obj");
			survivors_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("SURV0", std::move(loaded_model)));
		}


		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord0.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD0", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord1.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD1", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord2.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD2", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord3.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD3", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord4.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD4", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord5.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD5", std::move(loaded_model)));
		}

		{
			std::unique_ptr<Fabr::Graphics::Model> loaded_model = std::make_unique<Fabr::Graphics::Model>();
			loaded_model->loadFromFile("Assets/Models/ord6.obj");
			orders_models.insert(std::pair<std::string, std::unique_ptr<Fabr::Graphics::Model>>("ORD6", std::move(loaded_model)));
		}

	}

	void Assets::unloadAssets()
	{
		for (auto& ord : orders_models)
		{
			ord.second.reset();
		}
		orders_models.clear();

		for (auto& surv : survivors_models)
		{
			surv.second.reset();
		}
		survivors_models.clear();

		for (auto& poi : POI_models)
		{
			poi.second.reset();
		}
		POI_models.clear();

		for (auto& planet : planets_models)
		{
			planet.second.reset();
		}
		planets_models.clear();

		for (auto& sun : suns_models)
		{
			sun.second.reset();
		}
		suns_models.clear();

		player_model.reset();

		for (auto& shader : shaders)
		{
			shader.second.reset();
		}
		shaders.clear();
	}

	Assets* getAssets()
	{
		return assets.get();
	}
}