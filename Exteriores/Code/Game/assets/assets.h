#pragma once

#include <map>
#include <Engine/graphics/model/model.h>
#include <Engine/graphics/shader/shader.h>

namespace Extr::Assets
{
	class Assets
	{
	public:
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& getSunsModels();
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& getPlanetsModels();
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& getPOIModels();
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& getOrdersModels();
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>>& getSurvivorsModels();
		const std::unique_ptr<Fabr::Graphics::Model>& getPlayerModel();
		const std::map<std::string, std::unique_ptr<Fabr::Graphics::Shader>> & getShaders();


		void loadAssets();
		void unloadAssets(); // TERMIFIX

	private:
		std::map<std::string, std::unique_ptr<Fabr::Graphics::Shader>> shaders;

		std::unique_ptr<Fabr::Graphics::Model> player_model;

		std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>> suns_models;
		std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>> planets_models;
		std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>> POI_models;
		std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>> orders_models;
		std::map<std::string, std::unique_ptr<Fabr::Graphics::Model>> survivors_models;
	};

	Assets* getAssets();

}