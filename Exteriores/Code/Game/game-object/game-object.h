#pragma once

#include <Engine/faber-object/faber-object.h>

namespace Extr
{
	class GameObject : virtual public Fabr::FaberObject
	{
	public:
		GameObject() { std::cout << "GameObject()" << std::endl; }
		GameObject(const GameObject&) = default;
		GameObject(GameObject&&) = default;
		GameObject& operator=(const GameObject&) = default;
		GameObject& operator=(GameObject&&) = default;
		virtual ~GameObject() { std::cout << "~GameObject()" << std::endl; }
	};

}//namespace 
