#pragma once
#include <Game/game-object/game-object.h>
#include <Engine/graphics/shader/shader.h>
#include <Engine/transformable/transformable.h>
#include <Engine/graphics/model/model.h>
#include <Engine/graphics/light/light.h>


namespace Extr
{
	class GameActor : public virtual GameObject, public virtual Fabr::Math::Transformable
	{
	public:
		GameActor();
		explicit GameActor(glm::vec3 position);
		GameActor(const GameActor&) = default;
		GameActor(GameActor&&) = default;
		GameActor& operator=(const GameActor&) = default;
		GameActor& operator=(GameActor&&) = default;
		virtual ~GameActor() { std::cout << "~GameActor()" << std::endl; }


		virtual void render();

		void setShader(Fabr::Graphics::Shader* new_shader);
		void assignModel(Fabr::Graphics::Model * model);

		void setLightEmmitting(bool emiting);

	private:
		//model
		bool discovered;
		bool light_emiting;
		Fabr::Graphics::Shader	* shader;
		std::unique_ptr<Fabr::Graphics::PointLight>	light;
		Fabr::Graphics::Model * model;
	};
}