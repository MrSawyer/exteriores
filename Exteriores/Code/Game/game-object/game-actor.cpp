#include "game-actor.h"
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Game/assets/assets.h>

namespace Extr {

	GameActor::GameActor()
	{
		discovered = true;
		light_emiting = false;
		light = std::make_unique<Fabr::Graphics::PointLight>();
		model = nullptr;
		shader = nullptr;
	}

	GameActor::GameActor(glm::vec3 position)
	{
		setPosition(std::move(position));
		discovered = true;
		light_emiting = false;
		light = std::make_unique<Fabr::Graphics::PointLight>();

		model = nullptr;

		shader = nullptr;
	}

	void GameActor::setShader(Fabr::Graphics::Shader* new_shader)
	{
		shader = new_shader;
	}

	void GameActor::assignModel(Fabr::Graphics::Model* new_model)
	{
		model = new_model;
	}

	void GameActor::setLightEmmitting(bool emiting)
	{
		light_emiting = emiting;
	}

	void GameActor::render()
	{
		if (!discovered)return;
		if (shader == nullptr)return;
		if (model == nullptr)return;

		model->setPosition(getPosition());
		model->setScale(getScale());
		model->setRotation(getRotation());
		light_emiting = true;
		shader->use();

		if (light_emiting) {
			light->setPosition(getPosition());
			light->setPower(getScaleX() * 10);
	
			shader->send(light.get(), 0);
			shader->send(1, "u_pointlights_count");
			shader->send(1, "flipnormals");
		}
		else
		{
			shader->send(0, "u_pointlights_count");
			shader->send(0, "flipnormals");
		}

		shader->send(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition(), "u_cameraposition");


		model->draw();
	}
}
