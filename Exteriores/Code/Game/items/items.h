#pragma once

#include <Game/game-object/game-object.h>


namespace Extr
{

	/*
	* Silver star:
	* T1, T2, radar, kotwica
	* 
	* [0-20)[20-40)[40-60)[60-80)[80-85)[85-90)[90-95)[95-100)
	* 
	* Gold star:
	* T3, teleport, bomb, pulseinverter
	*/


	enum class ItemType
	{
		NONE, THRUSTER1, THRUSTER2, THRUSTER3, BOMB, ANCHOR, TELEPORT, PULSEINVERTER, RADAR
	};
}
