#pragma once

#include <Game/items/items.h>
#include <Engine/graphics/text/text.h>
#include <Engine/graphics/shader/shader.h>

namespace Extr {
	enum class GameState
	{
		INIT, IN_MENU, IN_GAME, IN_GAME_MENU, PAUSE, END_GAME, IN_CREDITS
	};

	class GameManager
	{
	public:
		static void initGame();
		static void terminateGame(); // TERMIFIX

		static void mainMenu();

		static void updateMenu();
		static void displayMenu();
		static void handleResize();

		static void updateLogic();
		static void renderGame();

		static void chooseItem(Extr::ItemType item);

		static void startGame();
		static void openMenu();
		
		static void openCredits();

		static void closeMenu();
		static void nextLevel();

		static void handleInput();

		static void handleCamera();

		static void increaseTakenSurvivors();

		static void drawPaths();

	private:
		inline static GameState state_ = GameState::IN_MENU;
		inline static unsigned int actual_level = 0;
		inline static Extr::ItemType choosed_item = ItemType::NONE;
		inline static unsigned int taken_survivors = 0;

		inline static std::unique_ptr<Fabr::Graphics::Font> font;
		inline static std::unique_ptr<Fabr::Graphics::Font> pause_font;
		inline static std::unique_ptr<Fabr::Graphics::Text> pause_text;
		inline static std::unique_ptr<Fabr::Graphics::Text> survivors_take_text;

		
		static void handleAddingOrders();
		/*
		Actualnie stawiany rozkaz:
		*/
		inline static bool adding_order = false;
		inline static glm::vec3 start_pos;
		inline static glm::vec3 destination;


	};
}