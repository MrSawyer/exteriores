#include "game-state.h"

#include <Game/menu/menu.h>
#include <Game/world/world.h>
#include <Game/player/player.h>
#include <Game/assets/assets.h>
#include <Engine/graphics/path-preset/path-preset.h>

#include <Engine/system/pipeline/pipeline-tick-variables.h>
//#include <Engine/system/input/input.h>

#include <cstdlib>

namespace Extr {

	std::unique_ptr<Fabr::Graphics::Shader> pathshader;

	void GameManager::initGame()
	{
		state_ = GameState::INIT;
		font = std::make_unique<Fabr::Graphics::Font>();
		pause_font = std::make_unique<Fabr::Graphics::Font>();
		pause_text = std::make_unique<Fabr::Graphics::Text>();
		survivors_take_text = std::make_unique<Fabr::Graphics::Text>();
		
		font->loadFromFile("Assets/Fonts/consolab.ttf", 24);
		pause_font->loadFromFile("Assets/Fonts/consolab.ttf", 48);

		pause_text->setFont(pause_font.get());
		pause_text->setText("GAME PAUSED");
		pause_text->setColor(1.0f, 1.0f, 1.0f);
		pause_text->setPositionX(-150);

		survivors_take_text->setFont(font.get());
		survivors_take_text->setColor(1.0f, 1.0f, 1.0f);

		Menu::generateMainMenu();
		Menu::generateInGameMenu();
		Menu::generateGameUI();
		Menu::generateEndGameScreen();
		Menu::generateCredits();


		Assets::getAssets()->loadAssets();
		getPlayer()->resetPlayer();

		pathshader = std::make_unique<Fabr::Graphics::Shader>();
		pathshader->loadFromFiles("Assets/Shaders/path-preset.vert", "Assets/Shaders/path-preset.frag");

		mainMenu();
	}

	void GameManager::terminateGame()
	{
		pathshader.reset();

		getPlayer()->resetPlayer();
		Assets::getAssets()->unloadAssets();

		Menu::terminateCredits();
		Menu::terminateEndGameScreen();
		Menu::terminateGameUI();
		Menu::terminateInGameMenu();
		Menu::terminateMainMenu();

		survivors_take_text.reset();
		pause_text.reset();
		pause_font.reset();
		font.reset();
	}

	void GameManager::mainMenu()
	{
		state_ = GameState::IN_MENU;
		nextLevel();
		actual_level = 0;

		Menu::displayMainMenu();
	}

	void GameManager::openCredits()
	{
		state_ = GameState::IN_CREDITS;
		Menu::displayCredits();
	}

	void GameManager::chooseItem(Extr::ItemType item)
	{
		choosed_item = item;
	}

	void GameManager::startGame()
	{
		Menu::hideInGameMenu();
		Menu::hideMainMenu();

		state_ = GameState::IN_GAME;
		Menu::displayGameUI();
	}

	void GameManager::openMenu()
	{
		state_ = GameState::IN_GAME_MENU;
		Menu::displayInGameMenu();
	}

	void GameManager::closeMenu()
	{
		state_ = GameState::IN_GAME;
		Menu::displayGameUI();

		Menu::hideInGameMenu();
		Menu::hideMainMenu();
	}

	void GameManager::nextLevel()
	{
		EQ old_eq;

		old_eq.anchor = 0;
		old_eq.bombs = 0;
		old_eq.pulse_inverter = 0;
		old_eq.radars = 0;
		old_eq.teleport = 0;
		old_eq.thruster[0] = 0;
		old_eq.thruster[1] = 0;
		old_eq.thruster[2] = 0;

		if (getPlayer())
		{
			old_eq = getPlayer()->getEQ();
		}
		getPlayer()->resetPlayer();

		getPlayer()->getEQ().anchor += old_eq.anchor;
		getPlayer()->getEQ().bombs += old_eq.bombs;
		getPlayer()->getEQ().pulse_inverter += old_eq.pulse_inverter;
		getPlayer()->getEQ().radars += old_eq.radars;
		getPlayer()->getEQ().teleport += old_eq.teleport;
		getPlayer()->getEQ().thruster[0] += old_eq.thruster[0];
		getPlayer()->getEQ().thruster[1] += old_eq.thruster[1];
		getPlayer()->getEQ().thruster[2] += old_eq.thruster[2];


		taken_survivors = 0;
		actual_level++;
		World::clearLevel();

		World::LevelSettings settings;
		settings.size_x = 10000 + (650 * actual_level);
		settings.size_y = 10000 + (650 * actual_level);

		settings.max_planet_size = 300;
		settings.min_planet_size = 100;

		settings.no_planets = 5 * actual_level;
		settings.no_survivours = 3 + (actual_level);
		settings.no_points_of_interest = 3  + (2 * actual_level);

		settings.min_distance = 1500;
		
		Menu::hideNextStageButton();

		World::getLevel()->generateLevel(settings);
	}

	void GameManager::updateLogic()
	{
		if (state_ == GameState::PAUSE)
		{
			handleInput();
			return;
		}

		if (state_ == GameState::IN_GAME)
		{
			handleInput();
			FASSERT(Extr::getPlayer() != nullptr);

			Extr::getPlayer()->update();
			
			if (getPlayer()->getState() == PlayerState::DESTROYED)
			{
				state_ = GameState::END_GAME;
				Menu::displayEndGameScreen();
			}

			return;
		}
	}


	void GameManager::renderGame()
	{
		if (state_ == GameState::IN_MENU)return;
		if (state_ == GameState::IN_CREDITS)return;

		if (state_ == GameState::END_GAME)return;

		if(getPlayer())getPlayer()->render();

		drawPaths();

		World::getLevel()->renderWorld();

	}

	void GameManager::updateMenu()
	{

		Menu::update();
	}

	void GameManager::displayMenu()
	{
		Menu::display();

		if (state_ == GameState::IN_GAME || state_ == GameState::PAUSE)Menu::displayTexts();

		if (state_ == GameState::PAUSE)
		{
			Assets::getAssets()->getShaders().find("TEXT")->second.get()->use();
			pause_text->draw();
		}


		if (state_ == GameState::IN_GAME || state_ == GameState::PAUSE) {
			if (taken_survivors >= (unsigned int)(World::getLevel()->getNumberOfGeneratedSurvivors() * 0.8f))
			{
				Menu::displayNextStageButton();
			}
			else
			{
				Assets::getAssets()->getShaders().find("TEXT")->second.get()->use();
				survivors_take_text->setText("Lost people found : " + std::to_string(taken_survivors) + " / " + std::to_string((unsigned int)(World::getLevel()->getNumberOfGeneratedSurvivors() * 0.8f)));
				survivors_take_text->draw();
			}
		}
	}

	void GameManager::handleInput()
	{
		/*
		* Je�li ESC to in game menu
		*/
		Fabr::System::Input* inputhandler = Fabr::System::PTVGlobalContainer::getActiveInput();
		if (inputhandler->keyReleased(VK_ESCAPE) && (state_ == GameState::IN_GAME || state_ == GameState::PAUSE))
		{
			openMenu();
		}

		/*
		* Jesli space to pause / unpause
		*/
		if (inputhandler->keyReleased(VK_SPACE))
		{
			if (state_ == GameState::IN_GAME)
			{
				state_ = GameState::PAUSE;
			}
			else
			{
				state_ = GameState::IN_GAME;
			}
		}

		if (Menu::isMouseOverGUI())return;


		/*
		* Jesli nie to IN GAME
		*/

		if (state_ == GameState::IN_GAME || state_ == GameState::PAUSE)
		{

			if (adding_order)
			{
				handleAddingOrders();
				return;
			}

			float factor = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition().y / inputhandler->getCursorDirection().y;
			glm::vec3 mouse3d = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition() - factor * inputhandler->getCursorDirection();

			if (inputhandler->keyReleased(Fabr::System::MouseButtons::RIGHT_BUTTON))
			{
				choosed_item = ItemType::NONE;
				Menu::disableGameUIHighlight();

				const Order * ord = World::getLevel()->collisionWithOrder(mouse3d, 1);
				if (ord != nullptr)
				{
					/*
					* Odda� graczowi EQ
					*/

					if (ord->type == ItemType::BOMB)getPlayer()->getEQ().bombs++;
					if (ord->type == ItemType::ANCHOR)getPlayer()->getEQ().anchor++;
					if (ord->type == ItemType::TELEPORT)getPlayer()->getEQ().teleport++;
					if (ord->type == ItemType::PULSEINVERTER)getPlayer()->getEQ().pulse_inverter++;

					World::getLevel()->destroyOrder(ord);
				}
			}

			if (inputhandler->keyPressed(Fabr::System::MouseButtons::LEFT_BUTTON) && choosed_item != ItemType::NONE)
			{
					/*
					* Sprawdzic czy mozna postawic
					*/

				if (choosed_item == ItemType::BOMB)
				{
					if (getPlayer()->getEQ().bombs == 0)return;
				}
				if (choosed_item == ItemType::TELEPORT)
				{
					if (getPlayer()->getEQ().teleport == 0)return;
				}
				if (choosed_item == ItemType::ANCHOR)
				{
					if (getPlayer()->getEQ().anchor == 0)return;
				}
				if (choosed_item == ItemType::PULSEINVERTER)
				{
					if (getPlayer()->getEQ().pulse_inverter == 0)return;
				}

				adding_order = true;
				start_pos = mouse3d;
			}
		}

		/*
		* Jesli wybrany order to lewy click new orde pos, a puszczony new order destination
		*/


	}

	void GameManager::handleCamera()
	{
		static const int CAMERA_SPEED = 100;
		static const float ZOOM = 5000.0f;


		if (state_ == GameState::IN_GAME)
		{
			Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setPosition(getPlayer()->getPosition().x - 300, ZOOM, getPlayer()->getPosition().z);
			Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setTarget(getPlayer()->getPosition());

		}

		if (state_ == GameState::PAUSE) {
			Fabr::System::Input* inputhandler = Fabr::System::PTVGlobalContainer::getActiveInput();

			if (inputhandler->keyPressed('W'))
			{
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setPositionX(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPositionX() + CAMERA_SPEED);
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setTargetX(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getTargetX() + CAMERA_SPEED);

			}
			if (inputhandler->keyPressed('S'))
			{
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setPositionX(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPositionX() - CAMERA_SPEED);
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setTargetX(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getTargetX() - CAMERA_SPEED);

			}
			if (inputhandler->keyPressed('D'))
			{
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setPositionZ(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPositionZ() + CAMERA_SPEED);
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setTargetZ(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getTargetZ() + CAMERA_SPEED);

			}
			if (inputhandler->keyPressed('A'))
			{
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setPositionZ(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPositionZ() - CAMERA_SPEED);
				Fabr::System::PTVGlobalContainer::getActiveCamera3D()->setTargetZ(Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getTargetZ() - CAMERA_SPEED);
			}
		}
	}

	void GameManager::increaseTakenSurvivors()
	{
		taken_survivors++;
	}

	void GameManager::drawPaths()
	{
		pathshader->use();
		std::vector<glm::vec3> path = Extr::getPlayer()->getPath();
		Fabr::Graphics::PathPreset::draw(path, glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), 30);

		for (unsigned int i=0; i<World::getLevel()->getPlayerOrders().size(); i++)
		{
			path = { World::getLevel()->getPlayerOrders()[i]->destination, World::getLevel()->getPlayerOrders()[i]->getPosition() };
			Fabr::Graphics::PathPreset::draw(path, glm::vec4(0.3f, 1.0f, 0.0f, 1.0f), 30);
		}

		if (adding_order)
		{
			Fabr::System::Input* inputhandler = Fabr::System::PTVGlobalContainer::getActiveInput();
			float factor = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition().y / inputhandler->getCursorDirection().y;
			glm::vec3 mouse3d = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition() - factor * inputhandler->getCursorDirection();
			path = { start_pos, mouse3d };
			Fabr::Graphics::PathPreset::draw(path, glm::vec4(0.3f, 0.0f, 1.0f, 1.0f), 30);
		}

	}

	void GameManager::handleAddingOrders()
	{
		Fabr::System::Input* inputhandler = Fabr::System::PTVGlobalContainer::getActiveInput();

		float factor = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition().y / inputhandler->getCursorDirection().y;
		glm::vec3 mouse3d = Fabr::System::PTVGlobalContainer::getActiveCamera3D()->getPosition() - factor * inputhandler->getCursorDirection();

		if (inputhandler->keyPressed(Fabr::System::MouseButtons::LEFT_BUTTON))return;

		float len = glm::length(mouse3d - start_pos);

		if (choosed_item == ItemType::BOMB || choosed_item == ItemType::THRUSTER1 || choosed_item == ItemType::THRUSTER2 
			|| choosed_item == ItemType::THRUSTER3 || choosed_item == ItemType::TELEPORT)
		{
			if (len <= 2 * glm::epsilon<float>())
			{
				adding_order = false;
				return;
			}
		}


		if (choosed_item == ItemType::BOMB)getPlayer()->getEQ().bombs--;

		if (choosed_item == ItemType::TELEPORT)getPlayer()->getEQ().teleport--;

		if (choosed_item == ItemType::ANCHOR)getPlayer()->getEQ().anchor--;

		if (choosed_item == ItemType::PULSEINVERTER)getPlayer()->getEQ().pulse_inverter--;


		World::getLevel()->addOrder(choosed_item, start_pos, mouse3d);

		adding_order = false;
	}

	void GameManager::handleResize()
	{
		Menu::resize();
		survivors_take_text->setPositionX(10.0f - Fabr::System::PTVGlobalContainer::getSurfaceWidth() / 2.0f);
		survivors_take_text->setPositionY(Fabr::System::PTVGlobalContainer::getSurfaceHeight()/2.0f - 30);

	}
} // namespace Extr::Game
