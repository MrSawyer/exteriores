#include "menu.h"

#include <Engine/flog/flog.h>
#include <Engine/system/pipeline/pipeline-tick-variables.h>
#include <Game/game.h>
#include <Engine/gui/gui.h>
#include <Game/assets/assets.h>
#include <Game/player/player.h>
#include <string>

namespace Extr::Menu {

	std::unique_ptr<Fabr::Gui::GUIObject> root_gui_main_menu;
	std::unique_ptr<Fabr::Gui::GUIObject> root_gui_ingame_menu;
	std::unique_ptr<Fabr::Gui::GUIObject> root_game;
	std::unique_ptr<Fabr::Gui::GUIObject> root_end_game;
	std::unique_ptr<Fabr::Gui::GUIObject> root_credits;


	std::unique_ptr<Fabr::Gui::Button> start_button;
	std::unique_ptr<Fabr::Gui::Button> credits_button;
	std::unique_ptr<Fabr::Gui::Button> exit_button;

	std::unique_ptr<Fabr::Gui::Button> continue_button;
	std::unique_ptr<Fabr::Gui::Button> back_button;

	std::unique_ptr<Fabr::Gui::Button> items[8];

	std::unique_ptr<Fabr::Gui::Button> next_stage;

	std::unique_ptr<Fabr::Graphics::Texture> new_game_tex;
	std::unique_ptr<Fabr::Graphics::Texture> credits_tex;
	std::unique_ptr<Fabr::Graphics::Texture> exit_tex;

	std::unique_ptr<Fabr::Graphics::Texture> continue_tex;
	std::unique_ptr<Fabr::Graphics::Texture> back_tex;

	std::unique_ptr<Fabr::Graphics::Texture> items_textures[8];
	std::unique_ptr<Fabr::Graphics::Texture> next_stage_texture;

	std::unique_ptr<Fabr::Graphics::Text> items_texts[8];
	std::unique_ptr<Fabr::Graphics::Font> font;

	std::unique_ptr<Fabr::Gui::GUIText> end_game_text;
	std::unique_ptr<Fabr::Gui::GUIText> end_game_desc;
	std::unique_ptr<Fabr::Gui::Button> end_game_button;
	std::unique_ptr<Fabr::Graphics::Font> font_primary;
	std::unique_ptr<Fabr::Graphics::Font> font_secondary;

	std::unique_ptr<Fabr::Gui::GUIText> credits_title;
	std::unique_ptr<Fabr::Gui::GUIText> credits_desc;
	std::unique_ptr<Fabr::Gui::Button> credits_back;

	void generateMainMenu()
	{
		root_gui_main_menu = std::make_unique<Fabr::Gui::GUIObject>();

		start_button = std::make_unique<Fabr::Gui::Button>();
		credits_button = std::make_unique<Fabr::Gui::Button>();
		exit_button = std::make_unique<Fabr::Gui::Button>();

		new_game_tex = std::make_unique<Fabr::Graphics::Texture>();
		credits_tex = std::make_unique<Fabr::Graphics::Texture>();
		exit_tex = std::make_unique<Fabr::Graphics::Texture>();

		continue_tex = std::make_unique<Fabr::Graphics::Texture>();
		back_tex = std::make_unique<Fabr::Graphics::Texture>();


		new_game_tex->loadFromFile("Assets/Textures/new_game.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		credits_tex->loadFromFile("Assets/Textures/credits.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		exit_tex->loadFromFile("Assets/Textures/exit.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		continue_tex->loadFromFile("Assets/Textures/continue.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		back_tex->loadFromFile("Assets/Textures/back.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);


		root_gui_main_menu->addChild(start_button.get());
		root_gui_main_menu->addChild(credits_button.get());
		root_gui_main_menu->addChild(exit_button.get());


		start_button->setOnClick(Extr::GameManager::startGame);
		credits_button->setOnClick(GameManager::openCredits);
		exit_button->setOnClick([]() {Fabr::System::PTVGlobalContainer::getActiveWindow()->close(); });

		start_button->setTexture(new_game_tex.get());
		credits_button->setTexture(credits_tex.get());
		exit_button->setTexture(exit_tex.get());

		resize();
	}

	void terminateMainMenu()
	{
		back_tex.reset();
		continue_tex.reset();

		exit_tex.reset();
		credits_tex.reset();
		new_game_tex.reset();

		exit_button.reset();
		credits_button.reset();
		start_button.reset();

		root_gui_main_menu.reset();
	}

	void displayMainMenu()
	{
		root_end_game->hide();
		root_gui_ingame_menu->hide();
		root_game->hide();
		root_gui_main_menu->show();
		root_credits->hide();

	}

	void hideMainMenu()
	{
		root_gui_main_menu->hide();
	}

	void resize()
	{
		
		if (!start_button || !credits_button || !exit_button || !continue_button || !back_button)return;
		for (unsigned int i = 0; i < 8; i++)
		{
			if (!items[i])return;
		}
		if (!end_game_text || !end_game_button || !end_game_desc)return;
		if (!credits_title || !credits_desc || !credits_back)return;

		float H = Fabr::System::PTVGlobalContainer::getSurfaceHeight();
		float W = Fabr::System::PTVGlobalContainer::getSurfaceWidth();

		start_button->setScale(glm::vec3(256, 144, 1));
		credits_button->setScale(glm::vec3(256, 144, 1));
		exit_button->setScale(glm::vec3(256, 144, 1));

		start_button->setPosition(glm::vec3(0, H / 4.0f, 1));
		credits_button->setPosition(glm::vec3(0, 0, 1));
		exit_button->setPosition(glm::vec3(0, - H / 4.0f, 1));

		continue_button->setScale(glm::vec3(256, 144, 1));
		back_button->setScale(glm::vec3(256, 144, 1));

		continue_button->setPosition(glm::vec3(0, H / 4.0f, 1));
		back_button->setPosition(glm::vec3(0, 0, 1));

		float size = 64;
		float margin = 10;

		for (unsigned int i = 0; i < 8; i++) {
			items[i]->setScale(glm::vec3(size, size, 1.0f));
			items[i]->setPosition(glm::vec3((i* size + (i+1)*(margin )) - (W / 2.0f) + (size / 2.0f), (-H/2.0f) + (1.05f * size), 1.0f));
			items_texts[i]->setPositionY(items[i]->getPositionY() - (size / 2.0f) - 2*margin);
			items_texts[i]->setPositionX(items[i]->getPositionX() - (size / 2.0f) + margin);

		}

		next_stage->setScale(glm::vec3(256, 144, 1));
		next_stage->setPosition(glm::vec3(W/2.0f - 150, H/2.0f - 100, 1));



		end_game_text->setPositionX(-500);
		end_game_text->setPositionY(200);
		end_game_desc->setPositionX(-500);
		end_game_desc->setPositionY(0);


		//end_game_desc;
		end_game_button->setPositionY(-H/6);
		end_game_button->setScaleX(256);
		end_game_button->setScaleY(144);

		// credits
		credits_title->setPositionX(-200);
		credits_title->setPositionY(300);

		credits_desc->setPositionX(-200);
		credits_desc->setPositionY(200);

		credits_back->setPositionY(-H / 3);
		credits_back->setScaleX(256);
		credits_back->setScaleY(144);
	}

	void update()
	{
		root_end_game->update();
		root_gui_main_menu->update();
		root_gui_ingame_menu->update();
		root_game->update();
		root_credits->update();

	}

	void displayTexts()
	{
		items_texts[0]->setText(std::to_string((unsigned int)getPlayer()->getEQ().thruster[0]));
		items_texts[1]->setText(std::to_string((unsigned int)getPlayer()->getEQ().thruster[1]));
		items_texts[2]->setText(std::to_string((unsigned int)getPlayer()->getEQ().thruster[2]));
		items_texts[3]->setText(std::to_string(getPlayer()->getEQ().bombs));
		items_texts[4]->setText(std::to_string(getPlayer()->getEQ().anchor));
		items_texts[5]->setText(std::to_string(getPlayer()->getEQ().teleport));
		items_texts[6]->setText(std::to_string(getPlayer()->getEQ().pulse_inverter));
		items_texts[7]->setText(std::to_string(getPlayer()->getEQ().radars));


		Assets::getAssets()->getShaders().find("TEXT")->second.get()->use();
		for (unsigned int i = 0; i < 8; i++)
		{
			items_texts[i]->draw();
		}
	}

	void display()
	{
		root_end_game->display();
		root_gui_main_menu->display();
		root_gui_ingame_menu->display();
		root_game->display();
		root_credits->display();
	}

	void generateInGameMenu()
	{
		root_gui_ingame_menu = std::make_unique<Fabr::Gui::GUIObject>();

		continue_button = std::make_unique<Fabr::Gui::Button>();
		back_button = std::make_unique<Fabr::Gui::Button>();


		root_gui_ingame_menu->addChild(continue_button.get());
		root_gui_ingame_menu->addChild(back_button.get());


		continue_button->setOnClick(Extr::GameManager::closeMenu);
		back_button->setOnClick(Extr::GameManager::mainMenu);

		continue_button->setTexture(continue_tex.get());
		back_button->setTexture(back_tex.get());

		resize();
	}

	void terminateInGameMenu()
	{
		back_button.reset();
		continue_button.reset();

		root_gui_ingame_menu.reset();
	}

	void displayInGameMenu()
	{
		root_end_game->hide();
		root_gui_main_menu->hide();
		root_game->hide();
		root_gui_ingame_menu->show();
		root_credits->hide();

	}

	void hideInGameMenu()
	{
		root_gui_ingame_menu->hide();
	}

	void disableGameUIHighlight()
	{
		for (unsigned int i = 0; i < 8; i++) {
			items[i]->setColor(glm::vec4(0.5,0.5,0.5,1));
		}
	}

	bool isMouseOverGUI()
	{
		bool over_gui = false;

		if (!root_game || !root_game->isVisible())return false;

		Fabr::System::Input* inputhandler = Fabr::System::PTVGlobalContainer::getActiveInput();
		if (!inputhandler)
		{
			FERR("No active input");
			return false;
		}

		if (next_stage->isPointInside(inputhandler->getCursorPosition()))return true;

		for (unsigned int i = 0; i < 8; i++) {
			if(items[i]->isPointInside(inputhandler->getCursorPosition()))return true;
		}

		return false;
	}

	void generateCredits()
	{
		root_credits = std::make_unique<Fabr::Gui::GUIObject>();
		credits_title = std::make_unique<Fabr::Gui::GUIText>();
		credits_desc = std::make_unique<Fabr::Gui::GUIText>();
		credits_back = std::make_unique<Fabr::Gui::Button>();

		credits_title->setFont(font_primary.get());
		credits_desc->setFont(font_secondary.get());

		credits_title->setText("Credits");
		credits_desc->setText("This game was made by \n\n       Michal Skarzynski (Sawyer) \n\n    and \n\n       Karol Balda (SirHipokryt) \n\n during PolyJam2021.");

		credits_back->setOnClick(GameManager::mainMenu);

		credits_back->setTexture(back_tex.get());

		root_credits->addChild(credits_title.get());
		root_credits->addChild(credits_desc.get());
		root_credits->addChild(credits_back.get());
	}

	void terminateCredits()
	{
		credits_back.reset();
		credits_desc.reset();
		credits_title.reset();
		root_credits.reset();
	}

	void displayCredits()
	{
		root_end_game->hide();
		root_gui_main_menu->hide();
		root_game->hide();
		root_gui_ingame_menu->hide();
		root_credits->show();
	}

	void hideCredits()
	{
		root_credits->hide();
	}

	void generateGameUI()
	{
		root_game = std::make_unique<Fabr::Gui::GUIObject>();

		font = std::make_unique<Fabr::Graphics::Font>();
		font->loadFromFile("Assets/Fonts/consolab.ttf", 24);

		for (unsigned int i = 0; i < 8; i++) {
			items[i] = std::make_unique < Fabr::Gui::Button>();
			
			items_texts[i] = std::make_unique<Fabr::Graphics::Text>();
			items_texts[i]->setFont(font.get());

			items_textures[i] = std::make_unique < Fabr::Graphics::Texture>();
			root_game->addChild(items[i].get());
		}



		next_stage_texture = std::make_unique < Fabr::Graphics::Texture>();
		next_stage_texture->loadFromFile("Assets/Textures/next_stage_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);


		items_textures[0]->loadFromFile("Assets/Textures/thruster0_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[1]->loadFromFile("Assets/Textures/thruster1_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[2]->loadFromFile("Assets/Textures/thruster2_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[3]->loadFromFile("Assets/Textures/bomb_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[4]->loadFromFile("Assets/Textures/anchor_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[5]->loadFromFile("Assets/Textures/teleport_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[6]->loadFromFile("Assets/Textures/reverter_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);
		items_textures[7]->loadFromFile("Assets/Textures/radar_ico.png", Fabr::Graphics::TextureImageType::TEXTURE_IMAGE);


		items[0]->setOnClick([]() {disableGameUIHighlight(); items[0]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::THRUSTER1); });
		items[1]->setOnClick([]() {disableGameUIHighlight(); items[1]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::THRUSTER2); });
		items[2]->setOnClick([]() {disableGameUIHighlight(); items[2]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::THRUSTER3); });
		items[3]->setOnClick([]() {disableGameUIHighlight(); items[3]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::BOMB); });
		items[4]->setOnClick([]() {disableGameUIHighlight(); items[4]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::ANCHOR); });
		items[5]->setOnClick([]() {disableGameUIHighlight(); items[5]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::TELEPORT); });
		items[6]->setOnClick([]() {disableGameUIHighlight(); items[6]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::PULSEINVERTER); });
		items[7]->setOnClick([]() {disableGameUIHighlight(); items[7]->setColor(glm::vec4(1, 1, 1, 1)); GameManager::chooseItem(ItemType::RADAR); });


		next_stage = std::make_unique < Fabr::Gui::Button>();
		next_stage->setOnClick(GameManager::nextLevel);
		root_game->addChild(next_stage.get());

		for (unsigned int i = 0; i < 8; i++) {
			items[i]->setTexture(items_textures[i].get());
		}

		next_stage->setTexture(next_stage_texture.get());

		resize();
	}

	void terminateGameUI()
	{
		next_stage.reset();
		next_stage_texture.reset();

		for (unsigned int i = 0; i < 8; ++i)
		{
			items_textures[i].reset();
			items_texts[i].reset();
			items[i].reset();
		}

		font.reset();

		root_game.reset();
	}


	void generateEndGameScreen()
	{
		root_end_game = std::make_unique<Fabr::Gui::GUIObject>();
		end_game_button = std::make_unique<Fabr::Gui::Button>();

		end_game_text = std::make_unique<Fabr::Gui::GUIText>();
		end_game_desc = std::make_unique<Fabr::Gui::GUIText>();

		font_primary = std::make_unique<Fabr::Graphics::Font>();
		font_secondary = std::make_unique<Fabr::Graphics::Font>();

		font_primary->loadFromFile("Assets/Fonts/consolab.ttf", 36);
		font_secondary->loadFromFile("Assets/Fonts/consolab.ttf", 24);

		end_game_text->setText("Now that you are on this screen \n   saving other people probably did not go as planned");
		end_game_desc->setText("Thank you for playing!");
		end_game_text->setFont(font_primary.get());
		end_game_desc->setFont(font_secondary.get());

		root_end_game->addChild(end_game_button.get());
		root_end_game->addChild(end_game_text.get());
		root_end_game->addChild(end_game_desc.get());

		end_game_button->setOnClick(Extr::GameManager::mainMenu);
		end_game_button->setTexture(back_tex.get());
		resize();

	}

	void terminateEndGameScreen()
	{
		font_secondary.reset();
		font_primary.reset();

		end_game_desc.reset();
		end_game_text.reset();

		end_game_button.reset();
		root_end_game.reset();
	}

	void displayEndGameScreen()
	{
		root_gui_main_menu->hide();
		root_game->hide();
		root_gui_ingame_menu->hide();
		root_end_game->show();
	}

	void displayGameUI()
	{
		root_end_game->hide();
		root_gui_main_menu->hide();
		root_game->show();
		root_gui_ingame_menu->hide();
	}

	void displayNextStageButton()
	{
		next_stage->show();
	}

	void hideNextStageButton()
	{
		next_stage->hide();
	}

}
