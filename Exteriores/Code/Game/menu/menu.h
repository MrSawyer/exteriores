#pragma once

namespace Extr::Menu {
	void generateMainMenu();
	void terminateMainMenu(); // TERMIFIX
	void displayMainMenu();
	void hideMainMenu();

	void resize();

	void update();
	void display();

	void displayTexts();

	void generateInGameMenu();
	void terminateInGameMenu(); // TERMIFIX
	void displayInGameMenu();
	void hideInGameMenu();

	void disableGameUIHighlight();

	bool isMouseOverGUI();

	void generateCredits();
	void terminateCredits(); // TERMIFIX
	void displayCredits();
	void hideCredits();

	void generateGameUI();
	void terminateGameUI(); // TERMIFIX
	void displayGameUI();

	void displayNextStageButton();
	void hideNextStageButton();

	void generateEndGameScreen();
	void terminateEndGameScreen(); // TERMIFIX
	void displayEndGameScreen();
}