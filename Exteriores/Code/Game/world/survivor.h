#pragma once
#include <Game/game-object/game-actor.h>


namespace Extr::World
{
	class Survivor : virtual public Extr::GameActor
	{
	public:
		Survivor() = default;
		explicit Survivor(glm::vec3 position);

		Survivor(const Survivor&) = default;
		Survivor(Survivor&&)noexcept = default;
		Survivor& operator=(const Survivor&) = default;
		Survivor& operator=(Survivor&&)noexcept = default;

		~Survivor() = default;
	};
}