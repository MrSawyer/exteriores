#pragma once

#include <Game/game-object/game-actor.h>

#include <Engine/transformable/transformable.h>
#include <Game/items/items.h>

namespace Extr::World
{

	enum class PointOfInterestType
	{
		SILVER, GOLD
	};

	class PointOfInterest : virtual public  Extr::GameActor
	{
	public:
		PointOfInterest() = default;
		explicit PointOfInterest(glm::vec3 position, PointOfInterestType type);

		PointOfInterest(const PointOfInterest&) = default;
		PointOfInterest(PointOfInterest&&)noexcept = default;
		PointOfInterest& operator=(const PointOfInterest&) = default;
		PointOfInterest& operator=(PointOfInterest&&)noexcept = default;

		~PointOfInterest() = default;

		const PointOfInterestType & getType() const;

		Extr::ItemType getItem() const;

	private:
		PointOfInterestType type_;
	};
}