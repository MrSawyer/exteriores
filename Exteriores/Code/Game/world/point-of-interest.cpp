#include "point-of-interest.h"


namespace Extr::World {
	PointOfInterest::PointOfInterest(glm::vec3 position, PointOfInterestType type)
		: GameActor(std::move(position))
	{
		type_ = type;
	}

	const PointOfInterestType& PointOfInterest::getType() const
	{
		return type_;
	}

	Extr::ItemType PointOfInterest::getItem() const
	{
		unsigned int rand_value = rand() % 100;

		if (type_ == PointOfInterestType::SILVER)
		{
			if (0 <= rand_value && rand_value < 20)return Extr::ItemType::THRUSTER1;
			if (20 <= rand_value && rand_value < 40)return Extr::ItemType::THRUSTER2;
			if (40 <= rand_value && rand_value < 60)return Extr::ItemType::RADAR;
			if (60 <= rand_value && rand_value < 80)return Extr::ItemType::ANCHOR;
			if (80 <= rand_value && rand_value < 85)return Extr::ItemType::THRUSTER3;
			if (85 <= rand_value && rand_value < 90)return Extr::ItemType::TELEPORT;
			if (90 <= rand_value && rand_value < 95)return Extr::ItemType::BOMB;
			if (95 <= rand_value && rand_value < 100)return Extr::ItemType::PULSEINVERTER;

		}
		if (type_ == PointOfInterestType::GOLD)
		{
			if (0 <= rand_value && rand_value < 20)return Extr::ItemType::THRUSTER3;
			if (20 <= rand_value && rand_value < 40)return Extr::ItemType::TELEPORT;
			if (40 <= rand_value && rand_value < 60)return Extr::ItemType::BOMB;
			if (60 <= rand_value && rand_value < 80)return Extr::ItemType::PULSEINVERTER;
			if (80 <= rand_value && rand_value < 85)return Extr::ItemType::THRUSTER1;
			if (85 <= rand_value && rand_value < 90)return Extr::ItemType::THRUSTER2;
			if (90 <= rand_value && rand_value < 95)return Extr::ItemType::RADAR;
			if (95 <= rand_value && rand_value < 100)return Extr::ItemType::ANCHOR;
		}

		return Extr::ItemType::THRUSTER1;
	}
}