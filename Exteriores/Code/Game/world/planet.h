#pragma once

#include <Game/game-object/game-actor.h>


namespace Extr::World {
	class Planet final : public Extr::GameActor
	{
	public:
		Planet() = default;
		explicit Planet(glm::vec3 position);

		Planet(const Planet&) = default;
		Planet(Planet&&)noexcept = default;
		Planet& operator=(const Planet&) = default;
		Planet& operator=(Planet&&)noexcept = default;

		~Planet() = default;
	};
}
