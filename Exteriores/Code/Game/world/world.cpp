#include "world.h"
#include <Engine/flog/flog.h>
#include <Game/assets/assets.h>
#include <GLM/gtc/epsilon.hpp>
#include <algorithm>
#include <functional>
namespace Extr::World {

	std::unique_ptr<Level> active_level = std::make_unique<Level>();

	static const unsigned int MAX_ITERATIONS = 500;

	void Level::generateLevel(LevelSettings settings)
	{
		settings_ = settings;
		
		generateSurvivous();
		generatePlanets();
		generatePOI();
	}

	glm::vec3 Level::calculateGravity(const glm::vec3& point)const
	{
		glm::vec3 gravity = glm::vec3(0, 0, 0);
		glm::vec3 dir_vec;

		for (unsigned int i = 0; i < planets_.size(); ++i) {
			
			dir_vec = planets_[i]->getPosition() - point;
			dir_vec = glm::normalize(dir_vec);

			float div = glm::pow(glm::distance(planets_[i]->getPosition(), point), 2);
			if (glm::abs(div) >  glm::epsilon<float>())dir_vec *= GRAVITY_CONSTANT * planets_[i]->getScaleX() / div;
			else
			{
				FERR("Gravity - dividing by zero");
				continue;
			}

			gravity += dir_vec;
		}

		return gravity;
	}

	bool Level::colisionWithPlanet(const glm::vec3& point, float radius) const
	{
		for (unsigned int i = 0; i < planets_.size(); ++i)
		{
			if (glm::distance(planets_[i]->getPosition(), point) <= 2.0f*(radius + planets_[i]->getScaleX()))return true;
		}
		return false;
	}

	const Survivor* Level::collisionWithSurvivors(const glm::vec3& point, float radius) const
	{
		for (unsigned int i = 0; i < survivors_.size(); ++i)
		{
			if (glm::distance(survivors_[i]->getPosition(), point) <= 4*(radius + survivors_[i]->getScaleX()))return survivors_[i].get();
		}
		return nullptr;
	}

	const PointOfInterest * Level::collisionWithPointOfInterest(const glm::vec3& point, float radius) const
	{
		for (unsigned int i = 0; i < points_of_interest_.size(); ++i)
		{
			if (glm::distance(points_of_interest_[i]->getPosition(), point) <= 4*(radius + points_of_interest_[i]->getScaleX()))return points_of_interest_[i].get();
		}
		return nullptr;
	}

	void Level::destroyPointOfInterest(const PointOfInterest* obj)
	{
		for (unsigned int i = 0; i < points_of_interest_.size(); ++i)
		{
			if (obj == points_of_interest_[i].get())
			{
				points_of_interest_.erase(points_of_interest_.begin() + i);
				return;
			}
		}
	}

	const Extr::Order * Level::collisionWithOrder(const glm::vec3& point, float radius)const
	{
		for (unsigned int i = 0; i < player_orders_.size(); ++i)
		{
			if (glm::distance(player_orders_[i]->getPosition(), point) <= 4.0f*(radius + 50))return player_orders_[i].get();
		}
		return nullptr;
	}

	void Level::destroyOrder(const Extr::Order* obj)
	{
		for (unsigned int i = 0; i < player_orders_.size(); ++i)
		{
			if (obj == player_orders_[i].get())
			{
				player_orders_.erase(player_orders_.begin() + i);
				return;
			}
		}
	}

	void Level::destroySurvivor(const Survivor* obj)
	{
		for (unsigned int i = 0; i < survivors_.size(); ++i)
		{
			if (obj == survivors_[i].get())
			{
				survivors_.erase(survivors_.begin() + i);
				return;
			}
		}
	}

	void Level::addOrder(Extr::ItemType type, glm::vec3 pos, glm::vec3 destination)
	{
		std::unique_ptr<Order> new_order = std::make_unique<Order>();
		new_order->type = type;
		new_order->setPosition(pos);
		new_order->setScale(150, 150, 150);
		new_order->destination = destination;
		new_order->setLightEmmitting(true);

		new_order->setShader(Assets::getAssets()->getShaders().begin()->second.get());

		if (type == ItemType::THRUSTER1)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD0")->second.get());
		if (type == ItemType::THRUSTER2)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD1")->second.get());
		if (type == ItemType::THRUSTER3)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD2")->second.get());
		if (type == ItemType::BOMB)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD3")->second.get());
		if (type == ItemType::ANCHOR)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD4")->second.get());
		if (type == ItemType::TELEPORT)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD5")->second.get());
		if (type == ItemType::PULSEINVERTER)new_order->assignModel(Assets::getAssets()->getOrdersModels().find("ORD6")->second.get());

		player_orders_.push_back(std::move(new_order));
	}

	void Level::renderWorld()
	{
		for (auto& p : planets_)
		{
			p->render();
			p->setRotationY(p->getRotationY() + 0.3f);

		}

		for (auto& p : points_of_interest_)
		{
			p->render();
			p->setRotationY(p->getRotationY() + 1);
		}

		for (auto& p : survivors_)
		{
			p->render();
			p->setRotationZ(p->getRotationZ() + 0.4);
			p->setRotationX(p->getRotationX() + 0.5);

		}

		for (auto& p : player_orders_)
		{
			p->render();
			p->setRotationY(p->getRotationY() + 0.4);

		}
	}

	unsigned int Level::getNumberOfGeneratedSurvivors()
	{
		return generated_survivors;
	}

	const std::vector<std::unique_ptr<Extr::Order>>& Level::getPlayerOrders() const
	{
		return player_orders_;
	}

	void Level::generateSurvivous()
	{
		for (unsigned int p = 0; p < settings_.no_survivours; ++p) {

			bool generated = false;

			for (unsigned int t = 0; t < MAX_ITERATIONS; t++) {
				glm::vec3 generated_pos(rand() % settings_.size_x, 0, rand() % settings_.size_y);

				bool can_generate = true;

				if (glm::distance(generated_pos, glm::vec3(0, 0, 0)) < settings_.min_distance)
				{
					can_generate = false;
					continue;
				}

				for (unsigned int i = 0; i < survivors_.size(); ++i)
				{
					if (glm::distance(generated_pos, survivors_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (can_generate)
				{
					//survivors_.push_back(Survivor(generated_pos));
					std::unique_ptr<Survivor> survivor = std::make_unique<Survivor>(generated_pos);

					auto rnd = Assets::getAssets()->getSurvivorsModels().begin();
					std::advance(rnd, rand() % Assets::getAssets()->getSurvivorsModels().size());
					survivor->assignModel(rnd->second.get());
					survivor->setScale(glm::vec3((float)100, (float)100, (float)100));

					survivor->setShader(Assets::getAssets()->getShaders().begin()->second.get());
					survivor->setRotationX(90);
					survivors_.push_back(std::move(survivor));

					generated = true;
					break;
				}

			}

			if (!generated)
			{
				FWAR("Cannot generate " << settings_.no_survivours << " survivors. Generated only " << p);
				break;
			}
		}

		generated_survivors = survivors_.size();
	}

	void Level::generatePlanets()
	{
		for (unsigned int p = 0; p < settings_.no_planets; ++p) {

			bool generated = false;

			for (unsigned int t = 0; t < MAX_ITERATIONS; t++) {
				glm::vec3 generated_pos(rand() % settings_.size_x, 0, rand() % settings_.size_y);
				unsigned int generate_size = rand() % (settings_.max_planet_size - settings_.min_planet_size) + settings_.min_planet_size;
				bool can_generate = true;

				if (glm::distance(generated_pos, glm::vec3(0, 0, 0)) < settings_.min_distance)
				{
					can_generate = false;
					continue;
				}

				for (unsigned int i = 0; i < survivors_.size(); ++i)
				{
					if (glm::distance(generated_pos, survivors_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (!can_generate)continue;

				for (unsigned int i = 0; i < planets_.size(); ++i)
				{
					if (glm::distance(generated_pos, planets_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (!can_generate)continue;

				if (can_generate)
				{
					std::unique_ptr<Planet> planet = std::make_unique<Planet>(generated_pos);

					auto rnd = Assets::getAssets()->getPlanetsModels().begin();
					std::advance(rnd, rand() % Assets::getAssets()->getPlanetsModels().size());
					planet->assignModel(rnd->second.get());
					planet->setScale(glm::vec3((float)generate_size, (float)generate_size, (float)generate_size));

					planet->setShader(Assets::getAssets()->getShaders().begin()->second.get());

					planets_.push_back(std::move(planet));
					generated = true;
					break;
				}

			}

			if (!generated)
			{
				FWAR("Cannot generate " << settings_.no_planets <<" planets. Generated only "<<p);
				break;
			}
		}

		/*
		* Choose suns
		*/
		unsigned int no_suns = planets_.size() * 0.3f;
		
		std::nth_element(planets_.begin(), planets_.begin() + no_suns, planets_.end(),
			[](const std::unique_ptr<Planet> & a, const std::unique_ptr<Planet> & b) -> bool
			{
				if (a->getScaleX() > b->getScaleX())return true;
				return false;
			}
		);

		for (unsigned int i = 0; i < no_suns; i++)
		{
			planets_[i]->setLightEmmitting(true);

			auto rnd = Assets::getAssets()->getSunsModels().begin();
			std::advance(rnd , rand() % Assets::getAssets()->getSunsModels().size());

			planets_[i]->assignModel(rnd->second.get());
		}


	}

	void Level::generatePOI()
	{
		for (unsigned int p = 0; p < settings_.no_points_of_interest; ++p) {

			bool generated = false;

			for (unsigned int t = 0; t < MAX_ITERATIONS; t++) {
				glm::vec3 generated_pos(rand() % settings_.size_x, 0, rand() % settings_.size_y);
				bool can_generate = true;

				if (glm::distance(generated_pos, glm::vec3(0, 0, 0)) < settings_.min_distance)
				{
					can_generate = false;
					continue;
				}

				for (unsigned int i = 0; i < survivors_.size(); ++i)
				{
					if (glm::distance(generated_pos, survivors_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (!can_generate)continue;

				for (unsigned int i = 0; i < planets_.size(); ++i)
				{
					if (glm::distance(generated_pos, planets_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (!can_generate)continue;


				for (unsigned int i = 0; i < points_of_interest_.size(); ++i)
				{
					if (glm::distance(generated_pos, points_of_interest_[i]->getPosition()) < settings_.min_distance)
					{
						can_generate = false;
						break;
					}
				}

				if (!can_generate)continue;


				if (can_generate)
				{
					PointOfInterestType POIType = PointOfInterestType::SILVER;
					if(rand()%100 > 70)POIType = PointOfInterestType::GOLD;
					std::unique_ptr<PointOfInterest> POI = std::make_unique<PointOfInterest>(std::move(generated_pos), std::move(POIType));
					POI->setScale(50, 50, 50);
					
					if (POIType == PointOfInterestType::SILVER)POI->assignModel(Assets::getAssets()->getPOIModels().find("POI0")->second.get());
					else POI->assignModel(Assets::getAssets()->getPOIModels().find("POI1")->second.get());

					POI->setRotationX(90);
					POI->setShader(Assets::getAssets()->getShaders().begin()->second.get());

					points_of_interest_.push_back(std::move(POI));
					generated = true;
					break;
				}

			}

			if (!generated)
			{
				FWAR("Cannot generate " << settings_.no_points_of_interest << " points of interest. Generated only "<<p);
				break;
			}
		}
	}

	Level* getLevel()
	{
		return active_level.get();
	}

	void clearLevel()
	{
		active_level.reset();

		active_level = std::make_unique<Level>();
	}

} // namespace Extr::World