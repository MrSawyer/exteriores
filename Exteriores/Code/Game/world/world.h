#pragma once

#include <Engine/faber-object/faber-object.h>
#include <Game/world/planet.h>
#include <Game/world/point-of-interest.h>
#include <Game/player/player-order.h>
#include <Game/world/survivor.h>


#include <vector>


namespace Extr::World
{
	const float GRAVITY_CONSTANT = 9.81f;

	struct LevelSettings : virtual GameObject
	{
		unsigned int size_x, size_y, no_survivours, no_planets, no_points_of_interest;
		float event_chance;
		float min_distance;
		unsigned int  min_planet_size, max_planet_size;
	};

	class Level final : virtual GameObject
	{
	public:
		void generateLevel(LevelSettings settings);

		glm::vec3 calculateGravity(const glm::vec3 & point) const;

		bool colisionWithPlanet(const glm::vec3& point, float radius)const;
		const Survivor * collisionWithSurvivors(const glm::vec3& point, float radius)const;


		const PointOfInterest * collisionWithPointOfInterest(const glm::vec3& point, float radius) const;

		void destroyPointOfInterest(const PointOfInterest* obj);

		const Extr::Order * collisionWithOrder(const glm::vec3& point, float radius) const;

		void destroyOrder(const Extr::Order * obj);
		void destroySurvivor(const Survivor * obj);

		void addOrder(Extr::ItemType type, glm::vec3 pos, glm::vec3 destination);

		void renderWorld();
		unsigned int getNumberOfGeneratedSurvivors();

		const std::vector<std::unique_ptr<Extr::Order>> & getPlayerOrders() const;

	private:
		void generateSurvivous();
		void generatePlanets();
		void generatePOI();

		unsigned int generated_survivors;

		LevelSettings settings_;

		std::vector< std::unique_ptr<Survivor>> survivors_;
		std::vector<std::unique_ptr<Planet>> planets_;
		std::vector< std::unique_ptr<PointOfInterest>> points_of_interest_;

		std::vector<std::unique_ptr<Extr::Order>> player_orders_;

	};

	Level * getLevel();
	void clearLevel();
}